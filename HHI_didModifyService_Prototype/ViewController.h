//
//  ViewController.h
//  HHI_didModifyService_Prototype
//
//  Created by Robert on 3/7/19.
//  Copyright © 2019 Robert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef  void (^SPHandler) (NSError *error);

@interface ViewController : UIViewController
- (IBAction)actCrash:(id)sender;

- (IBAction)btnECDSA:(id)sender;
- (IBAction)btnWrite:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segBLEPurpose;
@property (weak, nonatomic) IBOutlet UISwitch *switchStayConnected;
@property (weak, nonatomic) IBOutlet UITextField *txtCharacteristicUUID;

@property (copy, nonatomic) SPHandler bleHandler;
//4D050012-766C-42C4-8944-42BC98FC2D09
//@property (nonatomic, strong) CBCentralManager *mgr;
@property (weak, nonatomic) IBOutlet UILabel *LblResults;
@property (weak, nonatomic) IBOutlet UITextField *txtLockName;
@property (weak, nonatomic) IBOutlet UIButton *btnScan;
- (IBAction)actStartScanning:(id)sender;
-(void)updateUI:(NSError *)err;

@end

