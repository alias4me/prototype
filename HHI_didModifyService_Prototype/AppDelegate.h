//
//  AppDelegate.h
//  HHI_didModifyService_Prototype
//
//  Created by Robert on 3/7/19.
//  Copyright © 2019 Robert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

