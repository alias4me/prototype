//
//  main.m
//  HHI_didModifyService_Prototype
//
//  Created by Robert on 3/7/19.
//  Copyright © 2019 Robert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
