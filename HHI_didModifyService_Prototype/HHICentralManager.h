//
//  HHICentralManager.h
//  HHI_didModifyService_Prototype
//
//  Created by Robert on 3/8/19.
//  Copyright © 2019 Robert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

typedef  void (^SPHandler) (NSError *error);

@interface HHICentralManager : NSObject
@property (nonatomic, strong)CBCentralManager *mgr;
@property (nonatomic)CBPeripheral *lockPeripheral;
@property (copy, nonatomic) SPHandler bleHandler;
@property (nonatomic,strong)NSString *attributeTableChanged;
-(void)unlock;
-(void)startScanning:(NSString *)lockName sender:(id)senderVC;
-(void)readCharacteristics:(NSMutableArray <NSString *>*)characteristicUUIDs;
-(void)done;
-(void)crashBLE;
-(void)closeConnection;
-(void)writeIndication;
-(void)ECDSA;
@end

