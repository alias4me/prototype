//
//  ViewController.m
//  HHI_didModifyService_Prototype
//
//  Created by Robert on 3/7/19.
//  Copyright © 2019 Robert. All rights reserved.
//

#import "ViewController.h"
#import "HHICentralManager.h"

@interface ViewController ()
{
    HHICentralManager *mgr;
    CBPeripheral *lockPeripheral;
  //  NSString *attributeTableChanged;
    
}
@end


@implementation ViewController

@synthesize txtLockName;
@synthesize LblResults;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
   // mgr = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
    if (!mgr) {
        mgr=[[HHICentralManager alloc]init];
    }
    
    
}







- (IBAction)actStartScanning:(id)sender {
    if (self.segBLEPurpose.selectedSegmentIndex==0)
        [self scan];
    else if(self.segBLEPurpose.selectedSegmentIndex==1)
        [self lockRead];
    else if (self.segBLEPurpose.selectedSegmentIndex==0)
        [self lockWrite];
    
}

-(void)scan{
    _btnScan.enabled=false;
    // [self performSelector:@selector(done:) withObject:nil afterDelay:5];
    mgr.attributeTableChanged=@"Lock not Found";
    [mgr startScanning:txtLockName.text sender:self];
}

-(void)lockECDSA{
    
    [mgr ECDSA];
}

-(void)lockRead{
    NSString *uuidString;
    if ([[txtLockName.text substringToIndex:3] isEqualToString:@"SB:"]) {
        uuidString=[NSString stringWithFormat:@"%@%@%@",@"4D0500",_txtCharacteristicUUID.text, @"-766C-42C4-8944-42BC98FC2D09"];
    }else{
        
    }
    NSMutableArray *arrUUIDs=[[NSMutableArray alloc]initWithObjects:uuidString, nil];
    [mgr readCharacteristics:arrUUIDs];
}

-(void)lockWrite{
    
}

-(void)updateUI:(NSError *)err{
    LblResults.text=mgr.attributeTableChanged;
    if (_bleHandler) {
        _bleHandler(err);
        _bleHandler=nil;
        NSLog(@"handler returned");
    }else{
        NSLog(@"handler is nil");
    }
    _btnScan.enabled=true;
}







- (IBAction)actCrash:(id)sender {
    //[mgr crashBLE];
    [mgr unlock];
}

- (IBAction)btnECDSA:(id)sender {
}

- (IBAction)btnWrite:(id)sender {
    
    [mgr closeConnection];
}

- (IBAction)actWriteToIndication:(id)sender {
    [mgr writeIndication];
}


@end
