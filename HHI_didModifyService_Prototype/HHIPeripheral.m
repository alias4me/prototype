//
//  HHIPeripheral.m
//  HHI_didModifyService_Prototype
//
//  Created by Robert on 3/8/19.
//  Copyright © 2019 Robert. All rights reserved.
//

#import "HHIPeripheral.h"
#import "HHICentralManager.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface HHIPeripheral()<CBPeripheralDelegate>{
    HHICentralManager *bleManager;
    CBPeripheral *p;
    NSMutableArray<CBCharacteristic *> *lockCharacteristics;
    CharacteristicAction CharAction;
    CBCharacteristic *cmdTx;
    CBCharacteristic *cmdInd;
    NSData *d;
}
@end

@implementation HHIPeripheral


-(id)init{
    if (self == [super init]){

    }
    return self;
}

-(void)ECDSA:(HHICentralManager *)hhiMgr{
    
    bleManager=hhiMgr;
    bleManager.lockPeripheral.delegate=self;
    [bleManager.mgr connectPeripheral:bleManager.lockPeripheral options:nil];
}

-(void)connectHHILock:(HHICentralManager *)hhiMgr{ //HHILockCBCentralManager:(CBCentralManager *)mgr{
    bleManager=hhiMgr;
    bleManager.lockPeripheral.delegate=self;
    [bleManager.mgr connectPeripheral:bleManager.lockPeripheral options:nil];
}

-(void)scanServices:(CBPeripheral *)periph CharacteristicsHandler:(SPCharacteristicsHandler)characteristicsHandler{
    lockCharacteristics=[[NSMutableArray alloc]init];
    CharAction=CharacteristicScan;
    [periph discoverServices:nil];
    characteristicsHandler(lockCharacteristics,nil);
}

-(void)unlock{
    
    NSData *unlockData=[NSData dataWithBytes:(unsigned char[]){0x03,0x01,0x05} length:3];
    [p writeValue:unlockData forCharacteristic:cmdTx type:CBCharacteristicWriteWithResponse];

}

-(void)writeToIndication{
    NSData *unlockData=[NSData dataWithBytes:(unsigned char[]){0x01} length:1];
    [p writeValue:unlockData forCharacteristic:cmdInd type:CBCharacteristicWriteWithResponse];

}

-(void)writeToNothing{
    @try {
        NSData *unlockData=[NSData dataWithBytes:(unsigned char[]){0x03,0x01,0x05} length:3];
        [p writeValue:unlockData forCharacteristic:cmdTx type:CBCharacteristicWriteWithResponse];
    } @catch (NSException *exception) {
        NSLog(@"exceptio");
    } @finally {
        NSLog(@"finally");
    }
}

-(void)readCharacteristics:(NSArray <NSString *>*)uuids ReadHandler:(SPCharacteristicReadHandler)readHandler{
    
}

#pragma mark - CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray<CBService *> *)invalidatedServices
{
    bleManager.attributeTableChanged=@"Found change to Attribute Table";
   
    [bleManager done];
    //if ([sender isKindOfClass:[ViewController class]]) {
      //  ViewController *vc=sender;
        //[vc updateUI:nil];
    //}
    NSLog(@"FOUND MODIFIED SERVICES");
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    NSLog(@"updated notification state for %@", characteristic.UUID.UUIDString);
    
}

-(void)peripheralIsReadyToSendWriteWithoutResponse:(CBPeripheral *)peripheral{
    //not prefered as it only works on iOS 11.0 and above.
    //    [_delegate characteristicIsReadyToWrite:characteristic Peripheral:self];
    
    
}



-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    NSLog(@"did write to service uuid  %@", characteristic.UUID.UUIDString);
    
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        NSLog([NSString stringWithFormat:@"disc Svc error description:%@ code:%ld", error.description, error.code]);
    }else{
        for (CBService *service in peripheral.services) {
            NSLog([NSString stringWithFormat:@"discovered service %@", [service.UUID UUIDString]]);
            [peripheral discoverCharacteristics:nil forService:service];
        
        }
        
    }
        bleManager.attributeTableChanged=@"found services";
    //[bleManager.mgr cancelPeripheralConnection:bleManager.lockPeripheral];
        [bleManager done];
//    if ([sender isKindOfClass:[ViewController class]]) {
  //      ViewController *vc=sender;
    //    [vc updateUI:error];
    //}
    //    if ([sender respondsToSelector:@selector(done:)]) {
    //      [sender done:error];
    //}
    //    [sender done:error];
}


-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (error) {
        NSLog([NSString stringWithFormat:@"characteristic error description:%@ code:%ld", error.description, error.code]);
    }else{

        
        for (CBCharacteristic *ch in service.characteristics) {
            NSLog(@"characteristic found");
            NSLog([ch.UUID UUIDString]);
            CBCharacteristicProperties *prop = ch.properties;
            if (ch.properties == 0x28) {
                [peripheral setNotifyValue:true forCharacteristic:ch];
            }
           
            if ([[ch.UUID UUIDString] isEqualToString:@"4D050017-766C-42C4-8944-42BC98FC2D09"]) {
                NSLog(@"found CommandTx");
                //030105
                //if (unlock) {
                p=peripheral;
                cmdTx=ch;
                //}
            }else if ([[ch.UUID UUIDString] isEqualToString:@"4D050081-766C-42C4-8944-42BC98FC2D09"]) {
                NSLog(@"found Identify");
                [peripheral setNotifyValue:true forCharacteristic:ch];
                //030105
                //if (unlock) {
                NSData *identify=[NSData dataWithBytes:(unsigned char[]){0x01} length:1];
                [peripheral writeValue:identify forCharacteristic:ch type:CBCharacteristicWriteWithResponse];
                p=peripheral;
                cmdInd=ch;
                //}
            }else if ([[ch.UUID UUIDString] isEqualToString:@"4D050011-766C-42C4-8944-42BC98FC2D09"]) {
                NSLog(@"found system info characteristic");
                //[peripheral readValueForCharacteristic:ch];
                
            }
        }
    }
    
}





-(void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    // NSLog(@"hit descriptor");
}


-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error{
    NSLog(@"did update descriptor");
    
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    NSLog([NSString stringWithFormat:@"system info = %@",characteristic.value]);
    d=characteristic.value;
    if (d.length<10) {
        
        NSLog(@"data was nil... trying again");
        //[peripheral discoverServices:nil];
       // [self writeToNothing];
    }
}

@end
