//
//  SpectrumLockCharacteristics.m
//  SkyLocks
//
//  Created by Manoj Champanerkar on 4/1/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "SpectrumLockCharacteristics.h"
#import <Foundation/NSUUID.h>

@interface SpectrumLockCharacteristics(){
    NSDictionary *_configForAdvertisementData;
    NSDictionary <NSString*,NSString*> *_configForCharacSystemInfo;
    NSDictionary <NSString*,NSString*>*_configForCharacSystemSettingsInfo;
    NSDictionary <NSString*,NSString*>*_configForCharacSystemState;
    
}
@end


@implementation SpectrumLockCharacteristics
-(void)initCharacteristics
{
    _lockSystemCommandTx = [CBUUID UUIDWithString:UUID_LockSystem_CommandTx];
    _lockSYSTEM_INFO = [CBUUID UUIDWithString:UUID_SYSTEM_INFO];//[CBUUID UUIDWithString:UUID_SYSTEM_INFO];
    _lockSystem=[CBUUID UUIDWithString:UUID_LOCK_SYSTEM];
    _lockSystemState = [CBUUID UUIDWithString:UUID_SYSTEM_STATE];
    _lockSystemSettings=[CBUUID UUIDWithString:UUID_SYSTEM_SETTINGS];
    _lockAccessCodes=[CBUUID UUIDWithString:UUID_ACCESS_CODES];
    _lockEventLog=[CBUUID UUIDWithString:UUID_EVENT_LOG];
    _lockTimeRegion=[CBUUID UUIDWithString:UUID_TIME_REGION];
    _lockActivation=[CBUUID UUIDWithString:UUID_LOCK_ACTIVATION];
    _lockIdentify=[CBUUID UUIDWithString:UUID_IDENTIFY];
    _lockAuthentication=[CBUUID UUIDWithString:UUID_AUTHENTICATION];
    _lockCommissioningInfo=[CBUUID UUIDWithString:UUID_COMMISSIONING_INFO];
    _lockBLE_System=[CBUUID UUIDWithString:UUID_BLE_SYSTEM];
    _lockBondedMobilDeviceList=[CBUUID UUIDWithString:UUID_BONDED_MOBILE_DEVICE_LIST];
    _lockAdvertiseSettings=[CBUUID UUIDWithString:UUID_ADVERTISE_SETTINGS];
    _lockConnectionSettings=[CBUUID UUIDWithString:UUID_CONNECTION_SETTINGS];
    _lockTX_Power=[CBUUID UUIDWithString:UUID_TX_POWER];
    _lockBLEOTA=[CBUUID UUIDWithString:UUID_BLEOTA];
    _lockOTA_Update_Status=[CBUUID UUIDWithString:UUID_OTA_UPDATE_STATUS];
    
    //TO DO:PUT THESE IN JSON FILE AMD ENCRYPT IT
    //TLV8 (Type Length )- 1st byte is command type, 2nd byte is byte length of data
    _command_TxConfirmAuthentication=[[NSArray alloc]initWithObjects:@"102",@"0", nil];
    _command_TxSendChallenge=[[NSArray alloc]initWithObjects:@"98",@"32", nil];
    _command_TxSendSignature=[[NSArray alloc]initWithObjects:@"99",@"32", nil];
    _command_TxReqChallenge=[[NSArray alloc]initWithObjects:@"97",@"0", nil];
    _command_TxReqCert=[[NSArray alloc]initWithObjects:@"96",@"0", nil];
    _command_TxLock=[[NSArray alloc]initWithObjects:@"3",@"1",@"3", nil];
    _command_TxUnlock=[[NSArray alloc]initWithObjects:@"3",@"1",@"5", nil];
    _command_TxSetTime=[[NSArray alloc]initWithObjects:@"105",@"04",nil];
    _command_TxSetFriendlyName=[[NSArray alloc]initWithObjects:@"64",nil];
    _identify=[[NSArray alloc]initWithObjects:@"1", nil];
    
    
    _configForAdvertisementData=@{@"NotificationIndex":@"0,2,uint8",@"PairingFlag":@"2,1,uint8",@"ProtocolVer":@"3,1,uint8",@"ProductType":@"4,1,uint8",@"ProductID":@"4,2,uint8",@"CompanyID":@"6,2,uint8",@"UniqueID":@"8,9,uint8"};
    
    _configForCharacSystemInfo=@{@"ManufacturerDate":@"8,4,Epoch",@"WiFi_FirmwareBundle":@"12,1,uint8",@"WiFi_FirmwareMajor":@"13,1,uint8",@"WiFi_FirmwareMinor":@"14,1,uint8",@"WiFi_FirmwareBuild":@"15,1,uint8",@"BLE_FirmwareBundle":@"16,1,uint8",@"BLE_FirmwareMajor":@"17,1,uint8",@"BLE_FirmwareMinor":@"18,1,uint8",@"BLE_FirmwareBuild":@"19,1,uint8",@"VulcanFirmwareRev":@"20,2,ascii",@"VulcanHardwareRev":@"22,2,ascii",@"SKU":@"24,8,ascii"};//,@"ManufacturerDate":@"20,4,uint8"};//Epoch
    
    _configForCharacSystemState=@{@"BatteryAlarm":@"0,1,uint8",@"BatteryPercent":@"1,1,uint8",@"EventLog":@"2,4,uint8",@"AccessCodeListCRC":@"6,4,uint8",@"SecureModeStatus":@"10,1,uint8",@"DoorStatus":@"11,1,uint8",@"LockJammed":@"12,1,uint8",@"LockStatus":@"13,1,uint8"};//,@"ManufacturerDate":@"20,4,uint8"};//Epoch
    _configForCharacSystemSettingsInfo=@{@"SecureModeSchedule":@"0,7,Epoch",@"AutoLockTime":@"7,2,uint8",@"AutoLock":@"9,1,uint8",@"Buzzer":@"10,1,uint8",@"InteriorStatusLED":@"11,1,uint8",@"SecureScreen":@"12,1,uint8"};//,@"ManufacturerDate":@"20,4,uint8"};//Epoch
    
    
}

-(id)init
{
    if (self = [super init]) {
        [self initCharacteristics];
        
    }
    return self;
}


@end
