//
//  BLE_Data.h
//  Spectrum
//
//  Created by Robert on 4/24/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLE_Data : NSObject

-(NSData *)writeString:(NSString *)stringToWrite;
-(int)read_uint8_ValueFromBLE_Array:(NSString *)ConfigurationData NSDataOfArray:(NSData *)dataBytes;
//-(int)readHexInt:(NSString *)intHexToRead isLittleEndian:(BOOL)littleEndian;
-(NSString *)read_ascii_ValueFromBLE_Array:(NSString *)ConfigurationData NSDataOfArray:(NSData *)dataBytes;
-(NSDate *)read_EpochDate_ValueFromBLE_Array:(NSString *)ConfigurationData NSDataOfArray:(NSData *)dataBytes;
-(NSMutableData *)writeIntToBinaryHexFromArrayOfInt:(NSArray *)intArray;
-(NSString *)read_rawHex_ValueFromBLE_Array:(NSString *)ConfigurationData NSDataOfArray:(NSData *)dataBytes;
-(void)byteArray:(NSData *)hexData;
-(int)readHexInt:(NSString *)intHexToRead isLittleEndian:(BOOL)littleEndian;
-(NSString *)hexDataToString:(NSData *)hexData;
-(NSData *)writeHexDataFromASCIIstring:(NSString *)ASCIIstring;
@end
