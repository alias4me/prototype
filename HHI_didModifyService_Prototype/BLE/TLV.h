//
//  TLV.h
//  Spectrum
//
//  Created by Robert on 6/28/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLV : NSObject

-(id)initWithData:(NSData *)tlvData;
-(id)initWithDataAndUUID:(NSData *)tlvData characteristicUUID:(NSUUID *)uuid;

@property(nonatomic,assign)int packetType;
@property(nonatomic,assign)int packetLength;
@property(nonatomic,strong)NSData *packetValue;
@property(nonatomic,strong)NSData *endValue;


-(int)packetValueToInt;
@end
