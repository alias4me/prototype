//
//  BLE_Advertisement.h
//  Spectrum
//
//  Created by Robert on 5/9/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BLE_Advertisement;
@protocol BLEAdvertisementDelegate

-(void)catchAdvertisement:(BLE_Advertisement *)AdvData;
@end


@interface BLE_Advertisement : NSObject

@property(readonly,nonatomic)NSString *advertisedName;
@property(strong,nonatomic)NSString *uniqueID;
@property(readonly,nonatomic)NSString *companyID;
@property(readonly,nonatomic)NSString *productID;
@property(readonly,nonatomic)NSString *productType;
@property(readonly,nonatomic)NSString *protocolVer;
@property(readonly,nonatomic)NSString *pairingFlag;
@property(readonly,nonatomic)NSString *notificationIndex;
@property(readonly,nonatomic)NSNumber *RSSI;
@property(readonly,nonatomic)NSUUID *UUID_OfPeripheral;
@property(readonly,nonatomic)BOOL lockInBLE_Range;
@property(nonatomic)id <BLEAdvertisementDelegate> VCdelegate;

//-(void)checkIfLockIsInRange:(id <BLEAdvertisementDelegate>)delegate;
-(id)initWithDatabaseData:(NSString *)UniqueID ProductType:(NSString *)product_Type NotificationIndex:(NSString *)notifIndx FriendlyName:(NSString *)AdvertisedName PeripheralIdentifyer:(NSUUID *)peripheralUUID;
-(id)initWithData:(NSData *)ManufacturerData FriendlyName:(NSString *)AdvertisedName PeripheralIdentifyer:(NSUUID *)peripheralUUID Advertisement_RSSI:(NSNumber *)Adv_RSSI;
-(void)getBLE_Advertisement:(id <BLEAdvertisementDelegate>)VC;
-(void)discoveredAdvertisement:(NSMutableArray *)arOfAdvData;
@end
