//
//  BLEPeripheral+ReaderWriter.h
//  SkyLocks
//
//  Created by Manoj Champanerkar on 4/1/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "BLEPeripheral.h"

//@interface NSData(HexString)

//+(id)dataWithHexString:(NSString *)hex;

//- (NSString *)hexString;
//-(NSString *)hexToString:(NSString *)hexStringToConvert: (BOOL)isLittleEndian;

//@end

@interface BLEPeripheral (SpectrumLock)

//-(void)writeData:(NSData *)data forCharacteristic:(CBUUID *)uuid;
-(void)doLockUnLock:(bool)lock;
-(NSData *)readAuthentication;
-(void)requestCert;
-(void)requestChallenge:(CBCharacteristic*)lockCharacteristic;
-(void)identify:(NSArray *)dataToWrite Characteristic:(CBCharacteristic*)lockCharacteristic;
-(void)command_TX:(NSArray *)dataToWrite DataTypeForEncoding:(int)dataType Characteristic:(CBCharacteristic*)lockCharacteristic;
-(void)sendSignature:(CBCharacteristic*)lockCharacteristic appSignature:(NSData *)signature;
-(void)sendChallenge:(CBCharacteristic*)lockCharacteristic appChallenge:(NSData *)challenge;
-(void)confirmAuthentication:(CBCharacteristic*)lockCharacteristic;
-(void)setFriendlyName:(CBCharacteristic*)lockCharacteristic FriendlyName:(NSString *)friendlyName;

@end
