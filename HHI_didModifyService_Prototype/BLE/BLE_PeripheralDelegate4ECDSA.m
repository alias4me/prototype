//
//  BLE_PeripheralDelegate4ECDSA.m
//  Spectrum
//
//  Created by Robert on 5/31/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "BLE_PeripheralDelegate4ECDSA.h"
#import "BLEPeripheral+SpectrumLock.h"
#import "BLE_Data.h"
#import "TLV.h"
#import "ConstantErrorMessages.h"

@interface BLE_PeripheralDelegate4ECDSA(){
    NSMutableData *_signerCertificate, *_deviceCertificate, *_challenge4API, *_signature4API;
    NSMutableDictionary *_commandQueue;
   // id<BLE_PeripheralDelegate4ECDSADelegate> *_lock;
    BOOL _isPaired;
    NSMutableArray *serviceUUIDsAlreadyRead;
    NSMutableDictionary *dictCharcteistics;
    CBCharacteristic *cmd_Tx;
    NSString *_authenticationReason;
    int _authenticationResults;
    NSString *_errorMessage;
    NSString *_deviceID;
}
@end

enum ECDSA_WrReq{
    identfy,
    identifyDone,
    reqCert,
    readCert,
    reqChallenge,
    waitingForChallenge,
    sendSignature,
    sendChallenge,
    lock_Authenticated,
    done
};


int const authenticationSuccess=0;
int const requestCertTimeOut = 7;
int const sendCertFailure = 8;
int const requestChallengeTimeout = 9;
int const sendSignatureTimeOut = 10;
int const verifySignatureFailure = 11;
int const sendChallengeTimeOut = 12;
int const lockAuthenticatedTimeOut = 13;
int const protocolViolation = 15;
int const contextViolation = 16;
int const connectionFailed=100;
int const lockDidDisconnect=101;

@implementation BLE_PeripheralDelegate4ECDSA
//TODO: remove once we get cert data
NSFileManager *fileMgr;
    NSString *homeDir;
    NSString *filename;
    NSString *filepath;
//remove above

    NSString *testStr4Char=@"";
    BOOL refreshingCharacteristics=NO;
    enum ECDSA_WrReq ECDSA_request=identfy;
    BLEPeripheral *lockPeripheral;

    -(id)init{

    if (self=[super init]) {
        _commandQueue=[[NSMutableDictionary alloc]init];
        dictCharcteistics=[[NSMutableDictionary alloc]init];
        serviceUUIDsAlreadyRead=[[NSMutableArray alloc]init];
        _authenticationResults=-1;
  //      isIdentified=NO;
    }
    return self;
}



-(void)didFinish{
    //set activation date
    //executing this causes auth process to fail
     //write commands always happen before reads when executed synchronously
    /*
     NSDate *now = [NSDate date];
    NSTimeInterval nowEpochSeconds = [now timeIntervalSince1970];
    NSInteger today=nowEpochSeconds;
    NSMutableArray *ar=[[NSMutableArray alloc]initWithArray:SpectrumApplication.spectrumLockCharacterisitcs.command_TxSetTime];
        [ar addObject:[NSString stringWithFormat:@"%ld", today]];
    [lockPeripheral command_TX:ar DataTypeForEncoding:0 Characteristic:cmd_Tx];
    */

    //notify delegate that we finished
    [_lock ECDSA_ProcessFinished:lockPeripheral];
    //[SpectrumApplication.manager disconnect:periph];
}

-(void)identifyLockForECDSA_Process:(BLEPeripheral *)BLEPeripheralFromLock SPLock:(SPLocks *)lk{
    _lock=lk;
    ECDSA_request=identfy;
    lockPeripheral=BLEPeripheralFromLock;
    //we need to pair before we can start the authentication process
    BLEPeripheralFromLock.authenticationProcess=YES;
    [self ConnectToLock:BLEPeripheralFromLock VC_WithMethodsSetup:self ForcePairing:NO];

}


-(void)beginECDSAProcess:(BLEPeripheral *)BLEPeripheralFromLock{
    lockPeripheral=BLEPeripheralFromLock;
    BLEPeripheralFromLock.authenticationProcess=YES;
    
    DDLogDebug(@"about to execute reqestCert method");
    ECDSA_request=readCert;
  
    
    [BLEPeripheralFromLock requestCert];
    
    
   
//    [BLEPeripheralFromLock.peripheral discoverServices:arSvcLockActivation];
    //above

    //    [self ConnectToLock:BLEPeripheralFromLock VC_WithMethodsSetup:self ForcePairing:NO];
   // _certificate= [BLEPeripheralFromLock getCert];
    //
}


-(void)ConnectToLock:(BLEPeripheral *)BLEPeripheralFromLock VC_WithMethodsSetup:(id<BLEPeripheralDelegate>)delegate ForcePairing:(BOOL)forcePairing{
    //[peripheralsConnected
    // BLEPeripheralFromLock.periphera.identifier;
    
    BLEPeripheralFromLock.forcePairingByReadingCharacteristicValue=forcePairing;
    BLEPeripheralFromLock.delegate=delegate;
    //    [SpectrumApplication.manager connectToPeripheral:BLEPeripheralFromLock];
    
      [SpectrumApplication.manager connectToPeripheral:BLEPeripheralFromLock SetAuthentificationToNotify:BLEPeripheralFromLock.authenticationProcess];
    
}

-(void)addCommandInQueue:(CBUUID *)uuid writeData:(BOOL)doWrite dataToWrite:(NSArray*)data DataType:(int)data_Type{
    
    NSNumber *wrBool=[NSNumber numberWithBool:doWrite];
    NSMutableArray *arr4Wr=[[NSMutableArray alloc]init];
    [arr4Wr addObject:wrBool];
    [arr4Wr addObject:[NSNumber numberWithInt:data_Type]];
    [arr4Wr addObject:data];
    //    NSMutableArray *arr4Wr=[[NSMutableArray alloc]initWithObjects:wrBool,data_Type,data, nil];
    _commandQueue[uuid]=arr4Wr;
    
}


#pragma mark - BLEPeripheralDelegaet

-(void)didConnect:(BLEPeripheral *)peripheral
{
    //    __weak Robert_Temp_BLE_VC *wself = self;

    [peripheral discoverServices];

    [iOSHelper performActionOnMainThread:^{
        
        //wself.navigationItem.rightBarButtonItems = @[disconnectButton, refreshButton];
        //[_peripheralDetailTableView reloadData];
    }];
}

-(void)didDisConnect:(BLEPeripheral *)peripheral
{
     [self authenticationResults:lockDidDisconnect];
   
    //  __weak Robert_Temp_BLE_VC *wself = self;
    DDLogDebug(@"did disconnect from lock");
    [iOSHelper performActionOnMainThread:^{
        //wself.navigationItem.rightBarButtonItems = @[connectButton, refreshButton];
        //[_peripheralDetailTableView reloadData];
        
        //    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"did disconnect" message:@"Disconnected from Peripheral" preferredStyle:UIAlertControllerStyleAlert];
        //  UIAlertAction *alertAct=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * actClose) {}];
        //[alert addAction:alertAct];
        //[self presentViewController:alert animated:YES completion:nil];
    }];
}


-(void)didDiscoverCharacteristic:(CBCharacteristic *)ch Peripheral:(BLEPeripheral *)peripheral{
    dictCharcteistics[ch.UUID]=ch;
    
    if ([ch.UUID isEqual:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx]) {
        cmd_Tx=ch;
    }
    NSString *str1=[ch.UUID UUIDString];
    if (ECDSA_request==identfy) {
        
        if ((dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx]) && (dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockIdentify]) && (dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication])) {

        
           
            //[cAuth.service.peripheral setNotifyValue:YES forCharacteristic:cAuth];
            
            CBCharacteristic *c=dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockIdentify];
        
            [peripheral identify:SpectrumApplication.spectrumLockCharacterisitcs.identify Characteristic:c];
            ECDSA_request=identifyDone;
            
            CBCharacteristic *cAuth=dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
            
            [peripheral.peripheral readValueForCharacteristic:cAuth];
           
            
            
            //bellow forces bonding
           // ECDSA_request=reqCert;
           // [peripheral requestCert];
            

//        [peripheral.peripheral readValueForCharacteristic:cAuth];
            
//            NSArray <CBUUID*>*arSvcLockSys=[[NSArray alloc]initWithObjects:SpectrumApplication.spectrumLockCharacterisitcs.lockSystem, nil];
  //          [peripheral.peripheral discoverServices:arSvcLockSys];
            
            //BELLOW DID WORK minue setNotifyValue
/*
            CBCharacteristic *cAuth=dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
            [peripheral.peripheral setNotifyValue:YES forCharacteristic:cAuth];
            testStr4Char=[NSString stringWithFormat:@"%@ \n about to requestCert  \n",testStr4Char];
            [peripheral requestCert];
            NSDateFormatter *f=[[NSDateFormatter alloc]init];
            [f setDateFormat:@"HH:mm:ss"];
            testStr4Char=[NSString stringWithFormat:@"%@ \n did requestCert  \n %@",testStr4Char, [f stringFromDate:[NSDate date]]];
            
            NSArray <CBUUID*>*arSvcLockActivation=[[NSArray alloc]initWithObjects:SpectrumApplication.spectrumLockCharacterisitcs.lockActivation, nil];
           // ECDSA_request=sendChallenge;
            [peripheral.peripheral discoverServices:arSvcLockActivation];
//            [dictCharcteistics removeAllObjects];
  */
            //ABOVE DID WORK
        }
    }else if(ECDSA_request==readCert){
  
        
        if (dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication]){
            
            //[peripheral command_TX:SpectrumApplication.spectrumLockCharacterisitcs.command_TxReqCert DataTypeForEncoding:0 Characteristic:dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx]];
           // _certificate= [peripheral getCert];

           // ECDSA_request=sendChallenge;
//            [dictCharcteistics removeAllObjects];
        }

        if ([ch.UUID isEqual:SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication]) {
            //[peripheral.peripheral setNotifyValue:YES forCharacteristic:ch];
            
            BOOL isNotify=ch.isNotifying;
            if (isNotify) {
                DDLogDebug(@"is notifying, ");
            }else{
                DDLogDebug(@"not notify boo");
                
            }
            
           // if ((dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx]) && (ECDSA_request==reqCert)) {
                //[peripheral command_TX:SpectrumApplication.spectrumLockCharacterisitcs.command_TxReqCert DataTypeForEncoding:0 Characteristic:dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx]];
              //  [peripheral requestCert];
               // [dictCharcteistics removeAllObjects];
            //}

        }

    }else if (ECDSA_request==reqChallenge){
        ECDSA_request=waitingForChallenge;
        DDLogDebug(@"request challenge");
      
        
//        [peripheral requestChallenge:ch];

    }

}



-(void)didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)c{
    
}




-(void)characteristicDidWriteValue:(CBCharacteristic *)ch Peripheral:(BLEPeripheral *)peripheral{
    CBCharacteristic *cAuth=dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
    if (ECDSA_request==reqCert) {
            ECDSA_request=readCert;
    }else if (ECDSA_request==readCert){
        NSDateFormatter *f=[[NSDateFormatter alloc]init];
        [f setDateFormat:@"HH:mm:ss"];
        testStr4Char=[NSString stringWithFormat:@"%@ \n did write value  \n %@",testStr4Char, [f stringFromDate:[NSDate date]]];
        
        
        
        
        if (cAuth.isNotifying) {
            testStr4Char=[NSString stringWithFormat:@"%@ \n did write value IS NOTIFY  \n",testStr4Char];
            DDLogDebug(@"is now notifying");
           
        }else{
            testStr4Char=[NSString stringWithFormat:@"%@ \n did write value is not notify  \n",testStr4Char];
            DDLogDebug(@"Aithentication NOT notifying");
        }
    }
}

-(void)characteristicIsReadyToWrite:(BLEPeripheral *)peripheral{
//not currently used
    
}



-(void)didDiscoverServices:(BLEPeripheral *)peripheral Service:(CBService *)service
{
    
    if (refreshingCharacteristics) {
        DDLogDebug(@"discovered service for refreshing");
      //  [peripheral.peripheral discoverCharacteristics:nil forService:service];
    }
    
    // __weak Robert_Temp_BLE_VC *wself = self;
    if (ECDSA_request==identfy) {
        if ([service.UUID isEqual:SpectrumApplication.spectrumLockCharacterisitcs.lockActivation]) {
            NSArray <CBUUID*>*arAuthIdentify=[[NSArray alloc]initWithObjects:SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication,SpectrumApplication.spectrumLockCharacterisitcs.lockIdentify, nil];
            // ECDSA_request=sendChallenge;
            [peripheral.peripheral discoverCharacteristics:arAuthIdentify forService:service];
        }
 
    }
    DDLogInfo (@"discovered services: %lu", peripheral.services.count);
    
    if (([service.UUID isEqual:SpectrumApplication.spectrumLockCharacterisitcs.lockSystem]) &&(ECDSA_request==reqChallenge)) {
        
        //got cert now we need to request challenge
        
           NSArray <CBUUID *>*arrCharacteristics=[[NSMutableArray alloc]initWithObjects:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx, nil];
        
          [peripheral.peripheral discoverCharacteristics:arrCharacteristics forService:service];
        
    }

    
    if (([service.UUID isEqual:SpectrumApplication.spectrumLockCharacterisitcs.lockActivation]) &&(ECDSA_request==readCert)) {
        
        //got cert now we need to request challenge

     //   CBUUID *svc=SpectrumApplication.spectrumLockCharacterisitcs.lockSystem;
     //   NSArray <CBUUID *>*arrCharacteristics=[[NSMutableArray alloc]initWithObjects:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx, nil];
    
      //  [peripheral.peripheral discoverCharacteristics:arrCharacteristics forService:service];
      //  ECDSA_request=reqChallenge;
        
    }

}


-(void)catchCharacteristicValue:(CBCharacteristic *)c peripheral:(BLEPeripheral *)periph{
    DDLogInfo(@"caught value for characteristic %@   value=%@", [c.UUID UUIDString], c.value);
    //    NSArray *a=[dict allKeys];
    //CBCharacteristic *cAuth=dictCharcteistics[SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
   
   // NSString *str=[[NSString alloc]initWithData:d encoding:NSASCIIStringEncoding];
    
    //NSLog(@"characteristic value '%@'",d);
    
    NSDateFormatter *f=[[NSDateFormatter alloc]init];
    [f setDateFormat:@"HH:mm:ss"];
    testStr4Char=[NSString stringWithFormat:@"%@\n chatch characteristic  \n %@",testStr4Char, [f stringFromDate:[NSDate date]]];

    //if (periph.authenticationProcess) {
        if ([c.UUID isEqual:SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication]) {
//            NSData *d=c.value;;
            TLV *characteristicData=[[TLV alloc]initWithData:c.value];
            DDLogDebug(@"%@", [NSString stringWithFormat:@"Fragment packet=%@   fragment Type=%@  length=%d value=%@",c.value, characteristicData.packetValue,characteristicData.packetLength, characteristicData.packetValue]);
            
            //BLE_Data *decod=[[BLE_Data alloc]init];
           
         //   NSString *TLV_type= [decod read_rawHex_ValueFromBLE_Array:[NSString stringWithFormat:@"%@,1,uint8",typeStartPos] NSDataOfArray:d];
           // NSString *TLV_length= [decod read_rawHex_ValueFromBLE_Array:[NSString stringWithFormat:@"%@,1,uint8",lengthStartPos] NSDataOfArray:d];
            //NSString *TLV_value= @"";
            //if (d.length>2) {
              //  TLV_value= [decod read_rawHex_ValueFromBLE_Array:[NSString stringWithFormat:@"0,%@,uint8",valueEndPos] NSDataOfArray:d];
           // }
            if (characteristicData.packetType==03) {
                //authentication results
                [self authenticationResults:[characteristicData packetValueToInt]];
                return;
            }
            
            if (characteristicData.packetType ==20) {
                //end of signer cert
                [self sendCertToCloud:periph];
                ECDSA_request=waitingForChallenge;
                
                return;
            }
            
            if (ECDSA_request==identifyDone){
                [periph.peripheral setNotifyValue:YES forCharacteristic:c];
                [self didFinish];
            }else if (ECDSA_request== readCert) {
                //int certFragmentLength=d.length-2;
                if (characteristicData.packetType ==00) {
                    //Device Cert
                   //NSString *checkForEndOfDevCertification= [decod read_rawHex_ValueFromBLE_Array:@"0,2,uint8" NSDataOfArray:characteristicData.packetValue];
                    
                    [self appendFragmentToDeviceCert:characteristicData.packetValue];
                }else if (characteristicData.packetType ==04){
                    //Signer Cert
                   // NSString *checkForEndOfDevCertification= [decod read_rawHex_ValueFromBLE_Array:@"0,2,uint8" NSDataOfArray:d];
                    
                    [self appendFragmentToSignerCert:characteristicData.packetValue];

                    
                }
                
                
//                _certificate=testStr4Char;
                //[self didFinish];
            }else if (ECDSA_request==waitingForChallenge){
                //received challenge from lock
                DDLogDebug(@"Challenge data = '%@'", c.value);
                if (!_challenge4API) {
                    
                    if (characteristicData.packetValue) {
                        _challenge4API=[[NSMutableData alloc]initWithData:characteristicData.packetValue];
                    }
                    
                }else{
                    if (characteristicData.packetValue) {
                        [_challenge4API appendData:characteristicData.packetValue];
                    }
                }
                
                ECDSA_request=lock_Authenticated;
                [self sendChallengeToCloud:periph];
                //sendChallengeToCloud also sends Signature to BLE. no response from BLE w current firmware. I'm trying the send challenge r after as test
                
            }else if(ECDSA_request==sendChallenge){
                //TODO:I don't  believe this ever gets hit. test and remove
                DDLogDebug(@"signature data = '%@'", c.value);
               // d=[self removeTLVPayload:c.value];
                
                if (characteristicData.packetValue) {
                    _signature4API=characteristicData.packetValue;
                }
                
//                [periph sendChallenge:cmd_Tx appChallenge:nil];
                ECDSA_request=lock_Authenticated;
            }else if (ECDSA_request==lock_Authenticated){
                DDLogDebug(@"about to write Lock authenticated = '%@'", c.value);
                _signature4API=characteristicData.packetValue;
                

                //even though the authentication process is not completely finished if we don't change the BOOL value the discoverServices will trigger unneeded code execution
                periph.authenticationProcess=NO;
                periph.forcePairingByReadingCharacteristicValue=NO;

                //refreshingCharacteristics=YES;
                
                [self deviceAuthenticationToCloud:periph];
             
            }else if (ECDSA_request==done){
                DDLogDebug(@"authenticated=%@",characteristicData.packetValue);

                
            }else{
                DDLogDebug(@"data we are ubcertain of=%@  ECDSA_request=%d",characteristicData.packetValue,ECDSA_request);

            }
            
        }
    //}
}

-(void)appendFragmentToDeviceCert:(NSData *)certData{
    if (!_deviceCertificate) {
        if (certData) {
            _deviceCertificate=[[NSMutableData alloc]initWithData:certData];
        }
    }else{
        if (certData) {
//            [_deviceCertificate appendData:certData];
            [_deviceCertificate appendData:certData];
        }
    }
    DDLogDebug(@"_deviceCertificate value=%@", _deviceCertificate);
    DDLogDebug(@"device cert byte length= %lu",_deviceCertificate.length);
    
}


-(void)appendFragmentToSignerCert:(NSData *)certData{
    if (!_signerCertificate) {
        if (certData) {
            _signerCertificate=[[NSMutableData alloc]initWithData:certData];;
        }
    }else{
        if (certData) {
            [_signerCertificate appendData:certData];
            
        }
    }
    DDLogDebug(@"_signerCertificate value=%@", _signerCertificate);
    DDLogDebug(@"device cert byte length= %lu",_signerCertificate.length);
    
}



-(void)deviceAuthenticationToCloud:(BLEPeripheral *)periph{
    //send signature to cloud
    DDLogDebug(@"%@", [NSString stringWithFormat:@"signerCert=%@ \n deviceCert=%@ \n signature=%@",_signerCertificate, _deviceCertificate,_signature4API]);
    
    NSString *Base64Signature=[_signature4API base64EncodedStringWithOptions:0];
    //    NSString *Base64signerCertificate=[_signerCertificate base64EncodedStringWithOptions:0];
    NSDictionary * postDict = @{
                                @"digitalsignature": Base64Signature
                                };
    
    
    NSLog([NSString stringWithFormat:@"Base64 Signature=%@",Base64Signature]);
    
    
    
    NSString * urlString = [NSString stringWithFormat:DEVICE_AUTHENTICATION_TO_CLOUD_URL,_deviceID];
    
    NSString * url = [SpectrumApplication.bundleConfig.envConfig buildURL:urlString];
    
    NSMutableURLRequest * request = [SpectrumApplication.client buildRequestWithAuthForURL:url withHTTPType:@"POST"];
    
    
    [SpectrumApplication.client postJSON:postDict withRequest:request withCompletionHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
        [iOSHelper performActionOnMainThread:^{
            
        }];
        if(response.statusCode == 200) {
            
            if (data) {
                
                id dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                
                NSLog(@"verify deviceAuthenticationToCloud data response %@",dict);
                //need to send signature to LOCK
                [periph confirmAuthentication:cmd_Tx];
                ECDSA_request=done;
                
            }
            
            
            [iOSHelper performActionOnMainThread:^{
                
            }];
        }
        else {
            //TODO: REMOVE once API is worked out
            NSLog(@"FAILED TO verify deviceAuthenticationToCloud data response ");
            
            [periph confirmAuthentication:cmd_Tx];
            ECDSA_request=done;
            
        }
    }];
    
    
}

//GENERATE_CHALLENGE_URL

-(void)generateChallengeFromCloud:(BLEPeripheral *)periph{
    //generate challenge
    DDLogDebug(@"%@", [NSString stringWithFormat:@"signerCert=%@ \n deviceCert=%@ \n signature=%@",_signerCertificate, _deviceCertificate,_signature4API]);
    
   // NSString *Base64deviceChallenge=[_challenge4API base64EncodedStringWithOptions:0];
    //    NSString *Base64signerCertificate=[_signerCertificate base64EncodedStringWithOptions:0];
   // NSDictionary * postDict = @{};
    
    
    //NSLog([NSString stringWithFormat:@"Device Cert=%@",Base64deviceChallenge]);
    
    
    
    NSString * urlString = [NSString stringWithFormat:GENERATE_CHALLENGE_URL,_deviceID];
    
    NSString * url = [SpectrumApplication.bundleConfig.envConfig buildURL:urlString];
    
    NSMutableURLRequest * request = [SpectrumApplication.client buildRequestWithAuthForURL:url withHTTPType:@"GET"];
    
    
    [SpectrumApplication.client getJSONWithRequest:request withCompletionHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
        [iOSHelper performActionOnMainThread:^{
            
        }];
        if(response.statusCode == 200) {
            
            if (data) {
                
                id dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                
                NSData *challengeDataFromCloud =[dict[@"challenge"] dataUsingEncoding:NSUTF8StringEncoding];
                [periph sendChallenge:cmd_Tx appChallenge:challengeDataFromCloud];

                NSLog(@"generate Challenge from Cloud  data response %@",dict);
                //need to send signature to LOCK
            }
            
            
            [iOSHelper performActionOnMainThread:^{
                
            }];
        }
        else {
            //TODO: Replace with error codes.
            //NSData *challengeDataFromCloud =[dict[@"challenge"] dataUsingEncoding:NSUTF8StringEncoding];
            [periph sendChallenge:cmd_Tx appChallenge:nil];
            id dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"FAILED TO generate Challenge from Cloud  data response %@",dict);
        }
    }];
    
    
}


-(void)sendChallengeToCloud:(BLEPeripheral *)periph{
    //send challenge to cloud
    DDLogDebug(@"%@", [NSString stringWithFormat:@"signerCert=%@ \n deviceCert=%@ \n signature=%@",_signerCertificate, _deviceCertificate,_signature4API]);
    
    NSString *Base64deviceChallenge=[_challenge4API base64EncodedStringWithOptions:0];
    //    NSString *Base64signerCertificate=[_signerCertificate base64EncodedStringWithOptions:0];
    NSDictionary * postDict = @{
                                @"challenge": Base64deviceChallenge
                                };
    
    
    NSLog([NSString stringWithFormat:@"Device Cert=%@",Base64deviceChallenge]);
    
    
    
    NSString * urlString = [NSString stringWithFormat:CLOUD_AUTHENTICATION_DEVICE_URL,_deviceID];
    
    NSString * url = [SpectrumApplication.bundleConfig.envConfig buildURL:urlString];
    
    NSMutableURLRequest * request = [SpectrumApplication.client buildRequestWithAuthForURL:url withHTTPType:@"POST"];
    
    
    [SpectrumApplication.client postJSON:postDict withRequest:request withCompletionHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
        [iOSHelper performActionOnMainThread:^{
            
        }];
        if(response.statusCode == 200) {
            
            if (data) {
                
                id dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                
                NSLog(@"verify sendChallengeToCloud (signature) data response %@",dict);
                //need to send signature to LOCK
                NSData *signatureDataFromCloud =[dict[@"digitalsignature"] dataUsingEncoding:NSUTF8StringEncoding];
                //TODO:Convert signature from base64 to a format lock wants
                [periph sendSignature:cmd_Tx appSignature:signatureDataFromCloud];
               
                [self generateChallengeFromCloud:periph];
            }
            
            
            [iOSHelper performActionOnMainThread:^{
                
            }];
        }
        else {
            
        }
    }];
    
    
}



-(void)sendCertToCloud:(BLEPeripheral *)periph {
    //4.5
    [self readSampleCertData];
    NSString *Base64deviceCertificate=[_deviceCertificate base64EncodedStringWithOptions:0];
    NSString *Base64signerCertificate=[_signerCertificate base64EncodedStringWithOptions:0];
    NSDictionary * postDict = @{
                                 @"devicecert": Base64deviceCertificate,
                                @"factorycert": Base64signerCertificate
                                };
    
    
    NSLog([NSString stringWithFormat:@"Device Cert=%@",Base64deviceCertificate]);

    NSLog([NSString stringWithFormat:@"Signer Cert=%@",Base64signerCertificate]);
    
    
    NSString * urlString = [NSString stringWithFormat:DEVICE_CERT_VERIFICATION_URL,_deviceID];
    NSString * url = [SpectrumApplication.bundleConfig.envConfig buildURL:urlString];

    NSMutableURLRequest * request = [SpectrumApplication.client buildRequestWithAuthForURL:url withHTTPType:@"POST"];
    

    [SpectrumApplication.client postJSON:postDict withRequest:request withCompletionHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
        [iOSHelper performActionOnMainThread:^{

        }];
        if(response.statusCode == 200) {

            if (data) {
                
                id dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];

                NSLog(@"verify cert data response %@",dict);
                [periph requestChallenge:cmd_Tx];
                
            }
            
            [iOSHelper performActionOnMainThread:^{

            }];
        }
        else {
            
        }
    }];
    
}









-(void)authenticationResults:(int)authenticationCode{
  //  NSString *messageToShowUser=@"";

    if (authenticationCode ==authenticationSuccess) {
        _authenticationResults=authenticationSuccess;
        _authenticationReason=@"Success";
        
    }else if (authenticationCode ==requestCertTimeOut) {
        //req cert timed out
        _authenticationResults=requestCertTimeOut;
        _authenticationReason=@"Certificate timed out";
        ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
        _errorMessage=constantErrorMsgs.certificateTimeout;
        
    }else if (authenticationCode ==sendCertFailure) {
        //send cert failure
        DDLogDebug(@"send cert failure");
        _authenticationResults=sendCertFailure;
        _authenticationReason=@"Send certificate failure";
        ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
        _errorMessage=constantErrorMsgs.sendCertificateFailure;
        
    }else if (authenticationCode ==requestChallengeTimeout) {
        //req challenge timed out
        DDLogDebug(@"req challenge timed out");
        _authenticationResults=requestChallengeTimeout;
        _authenticationReason=@"Request challenge timed out";
        ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
        _errorMessage=constantErrorMsgs.requestChallengeTimeout;
        
    }else if (authenticationCode==sendSignatureTimeOut) {
        //send signature timed out
        DDLogDebug(@"send signature timed out");
        _authenticationResults=sendSignatureTimeOut;
        _authenticationReason=@"Send signature timed out";
        ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
        _errorMessage=constantErrorMsgs.sendSignatureTimeout;
        
    }else if (authenticationCode ==verifySignatureFailure) {
        //verify signature failure
        DDLogDebug(@"verify signature failure");
        _authenticationResults=verifySignatureFailure;
        _authenticationReason=@"Verify signature failure";
        ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
        _errorMessage=constantErrorMsgs.verifySignature;
        
    }else if (authenticationCode==sendChallengeTimeOut) {
        //send challenge timed out
        DDLogDebug(@"send challenge timed out");
        _authenticationResults=sendChallengeTimeOut;
        _authenticationReason=@"Send challenge timed out";
        ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
        _errorMessage=constantErrorMsgs.sendChallengeTimeout;
        
    }else if (authenticationCode ==lockAuthenticatedTimeOut) {
        //lock authenticated timed out
        DDLogDebug(@"lock authenticated timed out");
        _authenticationResults=lockAuthenticatedTimeOut;
        _authenticationReason=@"Lock authenticated timed out";
        ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
        _errorMessage=constantErrorMsgs.lockAuthenticationTimeout;
        
    }else if (authenticationCode==protocolViolation) {
        //protocol violation
        DDLogDebug(@"protocol violation");
        _authenticationResults=protocolViolation;
        _authenticationReason=@"Protocol violation";
        ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
        _errorMessage=constantErrorMsgs.protocolViolation;
        
    }else if (authenticationCode ==contextViolation) {
        //context violation
        DDLogDebug(@"context violation");
        _authenticationResults=contextViolation;
        _authenticationReason=@"Context violation";
        ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
        _errorMessage=constantErrorMsgs.contextViolation;
        
    }else if (connectionFailed){
        DDLogDebug(@"connection failed, make sure lock is not paired when attempting to authenticate");
        _authenticationResults=connectionFailed;
        _authenticationReason=@"Connection Failed";
        ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
        _errorMessage=constantErrorMsgs.failedConnection;
        
    }else if (lockDidDisconnect){
        DDLogDebug(@"lock disconnected, user is possibly out of range");
        if (!_authenticationResults) {
            //if there is no prior authentication result (like a timeout) then user likely stepped out of range.
            _authenticationResults=lockDidDisconnect;
            _authenticationReason=@"lock disconnected";
            ConstantErrorMessages *constantErrorMsgs=[[ConstantErrorMessages alloc]init];
            _errorMessage=constantErrorMsgs.disConnect;
            
        }

    }
    [self didFinish];

}


-(void)didFailToConnectToPeripheral:(BLEPeripheral *)peripheral error:(NSError *)error{
    [self authenticationResults:connectionFailed];
}


-(void)readSampleCertData{

    NSArray *paths = [[NSBundle mainBundle] pathForResource:@"sample_device" ofType:@"der"];
    NSArray *paths2 = [[NSBundle mainBundle] pathForResource:@"sample_signer" ofType:@"der"];
    //NSString *pth=paths[0];

    NSData *data = [[NSFileManager defaultManager] contentsAtPath:paths];
    NSData *data2 = [[NSFileManager defaultManager] contentsAtPath:paths2];
   // NSString *txtInFile = [[NSString alloc] initWithContentsOfFile:paths[0] encoding:NSUnicodeStringEncoding error:nil];
    _deviceCertificate=data;
    _signerCertificate=data2;
    
    
}
@end
