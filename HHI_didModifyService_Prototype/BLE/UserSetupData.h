//
//  FirstTimeSetupData.h
//  SkyLocks
//
//  Created by Robert on 4/9/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserSetupData : NSObject

-(id)initWithData:(NSString *)usrCognitoID FirstName:(NSString *)usrFirstNm LastName:(NSString *)usrLastNm Email:(NSString *)usrEml Phone:(NSString *)usrPhoneNumb IdentityProvider:(NSString *)identityProvider;

@property (readonly, nonatomic)NSString *email;
@property (readonly, nonatomic)NSString *firstName;
@property(readonly,nonatomic)NSString *lastName;
@property(readonly,nonatomic)NSString *phoneNumber;
@property(readonly,nonatomic)NSString *identityProvider;
@property(readonly,nonatomic)NSString *cognitoID;

-(void)updateDatabase;

@end
