//
//  BLE_Data.m
//  Spectrum
//
//  Created by Robert on 4/24/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "BLE_Data.h"


@implementation NSData (HexString)



+(id)dataWithHexString:(NSString *)hex
{
    char buf[3];
    buf[2] = '\0';
    NSAssert(0 == [hex length] % 2, @"Hex strings should have an even number of digits (%@)", hex);
    unsigned char *bytes = malloc([hex length]/2);
    unsigned char *bp = bytes;
    for (CFIndex i = 0; i < [hex length]; i += 2) {
        buf[0] = [hex characterAtIndex:i];
        buf[1] = [hex characterAtIndex:i+1];
        char *b2 = NULL;
        *bp++ = strtol(buf, &b2, 16);
        NSAssert(b2 == buf + 2, @"String should be all hex digits: %@ (bad digit around %d)", hex, i);
    }
    
    return [NSData dataWithBytesNoCopy:bytes length:[hex length]/2 freeWhenDone:YES];
}

-(NSString *)hexString
{
    /* Returns hexadecimal string of NSData. Empty string if data is empty.   */
    
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger          dataLength  = [self length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

@end

@interface BLE_Data(){
    
}
@end

@implementation BLE_Data

-(id)init{
    if (self = [super init]) {
        
        
        
    }//
    return self;
}

-(NSData *)writeInt:(int)intToWrite{
    //Encode int to HEX for BLE
    
    NSString *hex= [NSString stringWithFormat:@"%2lX",(unsigned long)intToWrite];
    
    DDLogInfo (@" converted int %d to hex %@ for BLE write", intToWrite,hex);
    
    return [NSData dataWithHexString:hex];
}

-(NSMutableData *)writeIntToBinaryHexFromArrayOfInt:(NSArray *)intArray{
    NSMutableData *retData=[[NSMutableData alloc]init];
    if (intArray) {
        if (intArray.count>0) {
            for (int i=0; i<intArray.count; i++) {
                [retData appendData:[self writeInt:[intArray[i] intValue]]];// ;
            }
        }
    }
    return retData;
}

-(NSData *)writeString:(NSString *)stringToWrite{
    //Encode string to hex for BLE

    //    NSString *retStr=[[NSString alloc]initWithData:dataToWrite encoding:NSASCIIStringEncoding];
    
    NSData *retData=[NSData dataWithHexString:stringToWrite];
    DDLogInfo (@" converting %@ to hex then inserting in NSData for BLE Write", stringToWrite);
    
    return retData;
}







-(int)readInt:(NSData *)intToRead{
    //Decodes hex Int from BLE and returns the int value it represented
    int retInt;
    if (intToRead) {
        NSString *dataString=[[NSString alloc]initWithData:intToRead encoding:NSASCIIStringEncoding];
        NSScanner *scanner=[NSScanner scannerWithString:dataString];
        [scanner scanHexInt:retInt];
        
        DDLogInfo (@" reading hex %@ from BLE and converted into int %@", dataString, dataString);
        
    }else{
        DDLogInfo (@" reading hex from BLE in method readInt is nil.");
        
    }
    return retInt;
}






-(NSString *)readString:(NSString *)hexStringToConvert LittleEndian:(BOOL)isLittleEndian{
    //Decodes hex ASCII from BLE and returns the String value it represented
    //Doesnt work.. 0800450200020000 correctly returns "E"
    //NSString *hexStringToConvert=[[NSString alloc]initWithData:stringToRead encoding:NSASCIIStringEncoding];
    NSMutableString *asciiString= [[NSMutableString alloc] initWithFormat:@""];
    
    if (hexStringToConvert) {
        
        int i = 0;
        while (i < [hexStringToConvert length])
        {
            NSString * hexChar = [hexStringToConvert substringWithRange: NSMakeRange(i, 2)];
            int value = 0;
            sscanf([hexChar cStringUsingEncoding:NSASCIIStringEncoding], "%x", &value);
            if (isLittleEndian) {
                asciiString = [NSString stringWithFormat:@"%c%@", (char)value, asciiString];
            }else{
                [asciiString appendFormat:@"%c", (char)value];
            }
            i+=2;
        }
    }else{
        DDLogInfo (@" reading hex from BLE in method readString is nil.");
        
    }
    
    //    htonl(asciiString);
    return asciiString;
}






-(BOOL)writeChar:(NSData *)charToWrite{
    BOOL retBool=YES;
    
    return retBool;
}

-(char *)readChar:(NSData *)charToRead{
    char retChar;
    
    return retChar;
}


-(NSString *)hexDataToString:(NSData *)hexData{
    return [hexData hexString];
}


-(int)readHexInt:(NSString *)intHexToRead isLittleEndian:(BOOL)littleEndian{
    //Decodes hex Int from BLE and returns the int value it represented
    int retInt=-1;
    @try{
        if (intHexToRead) {
            // NSString *dataString=[[NSString alloc]initWithData:intToRead encoding:NSASCIIStringEncoding];
            if (littleEndian) {
                NSString *reverseHexStringOrder=@"";
                for (int z=0; z<intHexToRead.length; z++) {
                    reverseHexStringOrder=[[intHexToRead substringWithRange:NSMakeRange(z, 2)] stringByAppendingString:reverseHexStringOrder];
                    z++;
                }
                intHexToRead=reverseHexStringOrder;
            }
            
            
            const char* hexstring=[[@"0x" stringByAppendingString:intHexToRead] UTF8String];
            retInt = (int)strtol(hexstring, NULL, 0);
            
        


            if (retInt==0) {
                
                DDLogDebug(@"value=0");
            }
            //DDLogInfo (@" reading hex %@ from BLE and converted into int %@", intHexToRead, intHexToRead);
            
        }else{
            DDLogInfo (@" reading hex from BLE in method readInt is nil.");
            
        }
    }@catch(NSException *ex){
        DDLogInfo(ex.reason);
        return retInt;
    }
    
    return retInt;
}





#pragma mark Public Decoding Methods
-(NSDate *)read_EpochDate_ValueFromBLE_Array:(NSString *)ConfigurationData NSDataOfArray:(NSData *)dataBytes{
    NSDate *retDate;
    int intDate;
    @try{
        NSString *arrayDataToRead=[dataBytes hexString];
        NSArray *arrPosAndLength=[ConfigurationData componentsSeparatedByString:@","];
        int startPos4Uint8=[arrPosAndLength[0] intValue];
        int lengthPos4Uint8=0;
        if ([arrPosAndLength[1] isEqualToString:@""]) {
            lengthPos4Uint8=(arrayDataToRead.length/2)-startPos4Uint8;
        }else{
            lengthPos4Uint8= [arrPosAndLength[1] intValue];
        }
        NSString *dataType=arrPosAndLength[2];
        int x=arrayDataToRead.length;
        int y=(startPos4Uint8*2)+(lengthPos4Uint8*2);
        if (x>=y)
        {
            
            arrayDataToRead=[arrayDataToRead substringWithRange:NSMakeRange(x-y, lengthPos4Uint8*2)];
            
            //        BLE_Data *encData=[[BLE_Data alloc]init];
            intDate= [self readHexInt:arrayDataToRead isLittleEndian:YES];
            
        }
    }@catch(NSException *ex){
        DDLogInfo(@"Error in decoding BLE data into uint8 %@", ex.reason);
        return retDate;
    }
    // if (intDate>-1) {
    retDate=[[NSDate alloc]initWithTimeIntervalSince1970:intDate];
    //}
    return retDate;
    
}

-(NSString *)read_rawHex_ValueFromBLE_Array:(NSString *)ConfigurationData NSDataOfArray:(NSData *)dataBytes{
    NSString *retStr=@"";
    
    if (ConfigurationData) {
        @try{
            
            NSString *arrayDataToRead=[dataBytes hexString];
            NSArray *arrPosAndLength=[ConfigurationData componentsSeparatedByString:@","];
            int startPos4Uint8=[arrPosAndLength[0] intValue];
            int lengthPos4Uint8=0;
            if ([arrPosAndLength[1] isEqualToString:@""]) {
                lengthPos4Uint8=(arrayDataToRead.length/2)-startPos4Uint8;
            }else{
                lengthPos4Uint8= [arrPosAndLength[1] intValue];
            }
            DDLogDebug(ConfigurationData);
         //   NSString *dataType=arrPosAndLength[2];
            int x=arrayDataToRead.length;
            int y=(startPos4Uint8*2)+(lengthPos4Uint8*2);
            if (x>=y)
            {
                
                retStr=[arrayDataToRead substringWithRange:NSMakeRange(x-y, lengthPos4Uint8*2)];
                
                //NSData *d = [NSData dataWithHexString:retStr];
                //  [self byteArray:d];
                
                
                //        BLE_Data *encData=[[BLE_Data alloc]init];
                //        BLE_Data *encData=[[BLE_Data alloc]init];
                //retInt= [self readHexInt:arrayDataToRead isLittleEndian:YES];
                
            }
        }@catch(NSException *ex){
            DDLogInfo(@"Error in decoding BLE data into uint8 %@", ex.reason);
            return @"";
        }
    }else{
        NSString *s=[[NSString alloc]initWithData:dataBytes encoding:NSUTF8StringEncoding];
        DDLogDebug(@"in BLE_Data read_uint8Value... Config=nil but data is %@",s);
    }
    return retStr;
    
}


-(void)byteArray:(NSData *)hexData{
    //Here:   NSData * fileData;
    uint8_t * bytePtr = (uint8_t  * )[hexData bytes];
    
    // Here, For getting individual bytes from fileData, uint8_t is used.
    // You may choose any other data type per your need, eg. uint16, int32, char, uchar, ... .
    // Make sure, fileData has atleast number of bytes that a single byte chunk would need. eg. for int32, fileData length must be > 4 bytes. Makes sense ?
    
    // Now, if you want to access whole data (fileData) as an array of uint8_t
    NSInteger totalData = [hexData length] / sizeof(uint8_t);
    
    for (int i = 0 ; i < totalData; i ++)
    {
        DDLogDebug(@"data byte chunk : %x", bytePtr[i]);
    }
}

-(int)read_uint8_ValueFromBLE_Array:(NSString *)ConfigurationData NSDataOfArray:(NSData *)dataBytes{
    int retInt=-1;
    if (ConfigurationData) {
        @try{
            
            NSString *arrayDataToRead=[dataBytes hexString];
            NSArray *arrPosAndLength=[ConfigurationData componentsSeparatedByString:@","];
            int startPos4Uint8=[arrPosAndLength[0] intValue];
            int lengthPos4Uint8=0;
            if ([arrPosAndLength[1] isEqualToString:@""]) {
                lengthPos4Uint8=(arrayDataToRead.length/2)-startPos4Uint8;
            }else{
                lengthPos4Uint8= [arrPosAndLength[1] intValue];
            }
            DDLogDebug(ConfigurationData);
           // NSString *dataType=arrPosAndLength[2];
            int x=arrayDataToRead.length;
            int y=(startPos4Uint8*2)+(lengthPos4Uint8*2);
            if (x>=y)
            {
                
                arrayDataToRead=[arrayDataToRead substringWithRange:NSMakeRange(x-y, lengthPos4Uint8*2)];
              
                retInt= [self readHexInt:arrayDataToRead isLittleEndian:YES];
                
            }
        }@catch(NSException *ex){
            DDLogInfo(@"Error in decoding BLE data into uint8 %@", ex.reason);
            return -1;
        }
    }else{
        NSString *s=[[NSString alloc]initWithData:dataBytes encoding:NSUTF8StringEncoding];
        DDLogDebug(@"in BLE_Data read_uint8Value... Config=nil but data is %@",s);
    }
    return retInt;
}

-(NSString *)read_ascii_ValueFromBLE_Array:(NSString *)ConfigurationData NSDataOfArray:(NSData *)dataBytes{
    NSString *retStr=@"";
    @try{
        NSString *arrayDataToRead=[dataBytes hexString];
        NSArray *arrPosAndLength=[ConfigurationData componentsSeparatedByString:@","];
        int startPos4Ascii=[arrPosAndLength[0] intValue];
        int lengthPos4Ascii=0;
        
        if ([arrPosAndLength[1] isEqualToString:@""]) {
            lengthPos4Ascii=(arrayDataToRead.length/2)-startPos4Ascii;
        }else{
            lengthPos4Ascii= [arrPosAndLength[1] intValue];
        }
        NSString *dataType=arrPosAndLength[2];
        int x=arrayDataToRead.length;
        int y=(startPos4Ascii*2)+(lengthPos4Ascii*2);
        if (x>=y)
        {
            
            arrayDataToRead=[arrayDataToRead substringWithRange:NSMakeRange(x-y, lengthPos4Ascii*2)];
            
            //        BLE_Data *encData=[[BLE_Data alloc]init];
            
            retStr=[self readString:arrayDataToRead LittleEndian:YES];
        }
    }@catch(NSException *ex){
        DDLogInfo(@"Error in decoding BLE data into ascii %@", ex.reason);
        return @"";
    }
    return retStr;
}

-(NSData *)writeHexDataFromASCIIstring:(NSString *)ASCIIstring{
    
    NSString * hexStr = [NSString stringWithFormat:@"%@",
                         [NSData dataWithBytes:[ASCIIstring cStringUsingEncoding:NSUTF8StringEncoding]
                                        length:strlen([ASCIIstring cStringUsingEncoding:NSUTF8StringEncoding])]];
    
    for(NSString * toRemove in [NSArray arrayWithObjects:@"<", @">", @" ", nil])
        hexStr = [hexStr stringByReplacingOccurrencesOfString:toRemove withString:@""];
    
    NSLog(@"%@", hexStr);
   
    
    
    NSData *dataFromString=[[NSData alloc]initWithBase64EncodedString:hexStr options:NSUTF8StringEncoding];
    return dataFromString;
}


-(NSData *)convertsStringToHexData:(NSString *)asciiString{
    NSMutableData *inputData = [[asciiString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES] mutableCopy];
    return inputData;
}
@end
