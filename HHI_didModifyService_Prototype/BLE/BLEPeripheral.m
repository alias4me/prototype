//
//  BLELock.m
//  SkyLocks
//
//  Created by Manoj Champanerkar on 3/21/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "BLEPeripheral.h"
#import "BLE_Data.h"
#import "BLE_Advertisement.h"

@interface BLEPeripheral ()<CBPeripheralDelegate>
{
    CBPeripheral *_peripheral;
    NSDictionary<NSString *,id> *_advertisementData;
    NSNumber *_rssi; // IS this needed? it an be obtained from peripheral!!
    NSMutableArray <CBService *>*_services;
    NSMutableDictionary <CBUUID *, CBCharacteristic *> *_characteristics;
    BOOL _forcePairingByReadingCharacteristicValue;
    BOOL _authenticationProcess;
}
@end

@implementation BLEPeripheral

NSString *str4Debugging;

-(id)initWithPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (self = [super init]) {
        _peripheral = peripheral;
        _advertisementData = advertisementData;
        _rssi = RSSI;
        _name = peripheral.name;
        _services = [[NSMutableArray alloc] init];
        _characteristics = [[NSMutableDictionary alloc] init];
        _forcePairingByReadingCharacteristicValue=NO;
        _authenticationProcess=NO;
        //if (!_name) _name = advertisementData[
    }
    return self;
}





-(void)connectToPeripheral:(CBPeripheral *)peripheral
{
    DDLogDebug(@"connecting to peripheral: %@(%@). discovering services...", peripheral.name, peripheral.identifier.UUIDString);
    [SpectrumApplication.manager registerPeripheral:self];
    _peripheral = peripheral;
    peripheral.delegate = self;
    //Executes method on delegate (VC) to notify VC when the execution is done
    [self.delegate didConnect:self];
    // no need to default servies.. let the delegate decide
}

-(void)disConnect
{
    // if (_peripheral.state == CBPeripheralStateConnected) {
    
    _peripheral.delegate = nil;
    [self.delegate didDisConnect:self];
    // }
}

-(void)discoverServices
{
      _peripheral.delegate=self;
    [_peripheral discoverServices:nil];
}

-(void)discoverServicesForUUIDs:(NSArray<CBUUID *>*)UUID_Array{
    _peripheral.delegate=self;
    [_peripheral discoverServices:UUID_Array];
}

-(bool) isLock
{
    //TODO inspect advertisement data or services if available ? TO check docu here
    return false;
}


-(NSString *)UUIDString
{
    return _peripheral.identifier.UUIDString;
}

-(CBCharacteristic *)characteristicForUUID:(CBUUID *)uuid
{
    CBCharacteristic * ret = _characteristics[uuid];
    if (ret == nil) {
        DDLogError (@"Peripheral: %@(%@) Characteristic:%@ not found!!!", self.name, self.UUIDString, uuid.UUIDString);
    }
    return ret;
}


#pragma mark - CBPeripheralDelegate
-(void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    NSLog(@"updated notification state for %@", characteristic.UUID.UUIDString);
  
    [_delegate didUpdateNotificationStateForCharacteristic:characteristic];
  
    
}

-(void)peripheralIsReadyToSendWriteWithoutResponse:(CBPeripheral *)peripheral{
//not prefered as it only works on iOS 11.0 and above.
    //    [_delegate characteristicIsReadyToWrite:characteristic Peripheral:self];
    [_delegate characteristicIsReadyToWrite:self];
    
    
}



-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
  
    NSLog(@"did write to service uuid  %@", characteristic.UUID.UUIDString);
    [_delegate characteristicDidWriteValue:characteristic Peripheral:self];
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error != nil)
        DDLogError (@"Peripheral: %@ - Discover Service Error : %@", peripheral.name, error);
    else {
        DDLogInfo (@"Peripheral: %@ - %lu services found", peripheral.name, peripheral.services.count);
        for (CBService *service in peripheral.services) {
            DDLogDebug (@"Peripheral: %@, Service: %@", peripheral.name, service.UUID.UUIDString);
            [peripheral discoverCharacteristics:nil forService:service];
            
        }
        
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (error != nil) {
        
    }
    else {
        DDLogInfo (@"Peripheral: %@, Service: %@, %lu - Characteristics found", peripheral.name, service.UUID.UUIDString,
                   service.characteristics.count);

        
        for (CBCharacteristic *ch in service.characteristics) {
            DDLogDebug  (@"Peripheral: %@, Service: %@, Characteristic: %@", peripheral.name, service.UUID.UUIDString, ch.UUID.UUIDString);
            
//            if (_forcePairingByReadingCharacteristicValue) {
  //              [peripheral readValueForCharacteristic:ch];
      //          _forcePairingByReadingCharacteristicValue=NO;
    //        }
            //              [peripheral readValueForCharacteristic:ch];
           // [peripheral discoverDescriptorsForCharacteristic:ch];
            //CBUUID *verifyUUID=[CBUUID UUIDWithString:@"4D050011-766C-42C4-8944-42BC98FC2D09"];
            
            
            //if ((_authenticationProcess) &&([SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication isEqual:ch.UUID])) {
            //if (_authenticationProcess){
            
//                [peripheral setNotifyValue:YES forCharacteristic:ch];
              
            //}
            
            _characteristics [ch.UUID] = ch;
            
            CBCharacteristic *ct = _characteristics[ch.UUID];
            DDLogDebug (@"%@ Found up characteristic %@", ct == nil ? @"Not":@"", ch.UUID.UUIDString);
            
            [_delegate didDiscoverCharacteristic:ch Peripheral:self];
            
        }
        
        if (![_services containsObject:service]) {
            [_services addObject:service];
        }

        //if (testBool) {
           // [self.delegate didDiscoverServices:self Service:service];
        //}
        
    }
    
}





-(void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
   // NSLog(@"hit descriptor");
}


-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error{
    NSLog(@"did update descriptor");
    
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    
    NSLog(@"didUpdateValueForCharacteristic for %@", [characteristic.UUID UUIDString]);
 //   id delgt=_delegate;
  //  if (([delgt isKindOfClass:[BLE_PeripheralDelegate4ECDSA class]])||([delgt isKindOfClass:[SPLocks class]])) {
        //NSData *dataBytes = characteristic.value;
        //NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        //dict[characteristic.UUID]=dataBytes;
        [_delegate catchCharacteristicValue:characteristic peripheral:self];
        
    //}
    
}



#pragma mark - Methods to extract data from _characteristics if populated
//TO DO: PAIR CONFIGURATION FILE WITH UUID CONSTANTS  MERGE GET<CHARACTERISTICS> METHODS AND USE CHARACTERISTIC AS CBUUID (OR UUID,CONFIG PAIR) ARGUMENT
-(BLE_Advertisement *)getAdvData{
    NSData *dataBytes = [_advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];
    BLE_Advertisement *advData=[[BLE_Advertisement alloc]initWithData:dataBytes FriendlyName:[_advertisementData objectForKey:CBAdvertisementDataLocalNameKey] PeripheralIdentifyer:_peripheral.identifier Advertisement_RSSI:_rssi];

    return advData;
}


-(NSMutableDictionary <CBUUID*,NSData*>*)getLockSystemState{
    
    str4Debugging=@"";
    NSMutableDictionary <CBUUID*,NSData*>*retDict;
    retDict= [self getLockCharDataByUUID_Config:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemState ConfigurationData:SpectrumApplication.spectrumLockCharacterisitcs.configForCharacSystemState];
    
    NSLog(str4Debugging);
    return retDict;
}

-(NSMutableDictionary <CBUUID*,NSData*>*)getLockSystemSettings{
    //
    
    str4Debugging=@"";
    NSMutableDictionary <CBUUID*,NSData*>*retDict;
    retDict= [self getLockCharDataByUUID_Config:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemSettings ConfigurationData:SpectrumApplication.spectrumLockCharacterisitcs.configForCharacSystemSettingsInfo];
    NSLog(str4Debugging);
    return retDict;
}

-(NSMutableDictionary <CBUUID*,NSData*>*)getLockCharDataByUUID_Config:(CBUUID *)cUUID ConfigurationData:(NSDictionary *)dictConfigData{
    if (!str4Debugging) {
        str4Debugging=@"";
    }
    
    BLE_Data *bleDataDecode=[[BLE_Data alloc]init];
    
    NSMutableDictionary *retDict=[[NSMutableDictionary alloc]initWithCapacity:dictConfigData.count];
    
    NSData *characData;
    if (_characteristics.count>0) {
        
        CBCharacteristic *c=_characteristics[cUUID];
        characData=c.value;
        //FOR TESTING
        NSString *strNSDATAForDebugging=[[NSString alloc]initWithData:characData encoding:NSUTF8StringEncoding];
        str4Debugging=[NSString stringWithFormat:@"%@ \n %@",str4Debugging,strNSDATAForDebugging];
        //FOR TESTING
        DDLogInfo(@"CHARACTER VALUES  UUID=%@ and data=%@",SpectrumApplication.spectrumLockCharacterisitcs.lockSystemSettings, characData);
        for (NSString *key in  dictConfigData) {
            @try{
                NSString *dictVal=dictConfigData[key];
                NSArray *arrOfConfig=[dictVal componentsSeparatedByString:@","];
                
                if ([arrOfConfig[2] isEqualToString:@"uint8"]) {
                    int intValOfData=[bleDataDecode read_uint8_ValueFromBLE_Array:dictVal NSDataOfArray:characData];
                    DDLogInfo(@"decoded uint8 %d", intValOfData);
                    //4 TESTING
                    str4Debugging=[NSString stringWithFormat:@"%@ \n %@     %@   returnedValue=%d",str4Debugging,key,dictVal,intValOfData];
                    //4 TESTING
                    if (intValOfData>-1) {
                        
                        [retDict setObject:[NSString stringWithFormat:@"%d", intValOfData] forKey:key];
                    }else{
                        DDLogInfo(@"ERROR intValOfData is nil %@",key);
                        
                    }
                    
                }else if ([arrOfConfig[2] isEqualToString:@"ascii"]){
                    NSString *strValOfData;
                    strValOfData=[bleDataDecode read_ascii_ValueFromBLE_Array:dictVal NSDataOfArray:characData];
                    //4 TESTING
                    str4Debugging=[NSString stringWithFormat:@"%@ \n %@    %@  returnedValue=%@",str4Debugging,key,dictVal,strValOfData];
                    //4 TESTING
                    
                    if (strValOfData) {
                        [retDict setObject:strValOfData forKey:key];
                    }else{
                        DDLogInfo(@"ERROR strValOfData in nil %@",key);
                        DDLogInfo(@"ERROR strValOfData in nil %@",key);
                        DDLogInfo(@"ERROR strValOfData in nil %@",key);
                        DDLogInfo(@"ERROR strValOfData in nil %@",key);
                        
                    }
                }else if ([arrOfConfig[2] isEqualToString:@"Epoch"]){
                    NSDate *dateOfData=[bleDataDecode read_EpochDate_ValueFromBLE_Array:dictVal NSDataOfArray:characData];
                    // DDLogInfo(@"decoded uint8 %d", dateOfData);
                    if (dateOfData) {
                        
                        NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
                        [dateFormat setDateFormat:@"MM/dd/yyyy"];
                        //4 TESTING
                        str4Debugging=[NSString stringWithFormat:@"%@ \n %@     %@   returnedValue=%@",str4Debugging,key,dictVal,[dateFormat stringFromDate:dateOfData]];
                        //4 TESTING
                        
                        [retDict setObject:[dateFormat stringFromDate:dateOfData] forKey:key];
                    }else{
                        DDLogInfo(@"Date is nil");
                    }
                    
                }else{
                    DDLogInfo(@"something went wrong in get sys info of BLEperipheral");
                }
                
            }@catch(NSException *ex){
                DDLogInfo(@"ERROR in Decoding %@ %@",key,ex.reason);
            }
        }
    }
    return retDict;
    
}

-(NSMutableDictionary <CBUUID*,NSData*>*)getLockSystemInfo{
    str4Debugging=@"";
    
    NSMutableDictionary <CBUUID*,NSData*>*retDict= [self getLockCharDataByUUID_Config:SpectrumApplication.spectrumLockCharacterisitcs.lockSYSTEM_INFO ConfigurationData:SpectrumApplication.spectrumLockCharacterisitcs.configForCharacSystemInfo];
    
    
    NSLog(str4Debugging);
    return retDict;
    
}

@end
