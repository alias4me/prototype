//
//  BLE_PeripheralDelegate4ECDSA.h
//  Spectrum
//
//  Created by Robert on 5/31/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEPeripheral+SpectrumLock.h"
//#import "SPLocks.h"

@class BLE_PeripheralDelegate4ECDSA;

@protocol BLE_PeripheralDelegate4ECDSADelegate

-(void)ECDSA_ProcessFinished:(BLEPeripheral *)Periph;

@end


@interface BLE_PeripheralDelegate4ECDSA : NSObject
-(id)init;


@property(strong,nonatomic)NSMutableData *deviceCertificate;
@property(strong,nonatomic)NSMutableData *signerCertificate;
@property(strong,nonatomic)NSMutableData *challenge4API;
@property(strong,nonatomic)NSMutableData *signature4API;
@property(strong,nonatomic)NSString *deviceID;

@property(assign, nonatomic)int authenticationResults;
@property(readonly,nonatomic)NSString *authenticationReason;
@property(readonly, nonatomic)NSString *errorMessage;

@property (strong,nonatomic)NSMutableDictionary *commandQueue;
@property(readonly,nonatomic)BOOL isPaired;
@property(nonatomic)__weak id<BLE_PeripheralDelegate4ECDSADelegate>lock;

-(void)identifyLockForECDSA_Process:(BLEPeripheral *)BLEPeripheralFromLock SPLock:(id)lk;
-(void)beginECDSAProcess:(BLEPeripheral *)BLEPeripheralFromLock;

-(void)didDiscoverCharacteristic:(CBCharacteristic *)ch Peripheral:(CBPeripheral *)peripheral;
-(void)characteristicDidWriteValue:(CBCharacteristic *)ch Peripheral:(BLEPeripheral *)peripheral;
-(void)characteristicIsReadyToWrite:(BLEPeripheral *)peripheral;

@end
