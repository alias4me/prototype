//
//  ErrorMessages.m
//  Spectrum
//
//  Created by Robert on 7/4/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "ConstantErrorMessages.h"

@interface ConstantErrorMessages(){
    NSString *_disConnect;
    NSString *_failedConnection;
    
    NSString *_certificateTimeout;
    NSString *_sendCertificateFailure;
    NSString *_requestChallengeTimeout;
    NSString *_sendSignatureTimeout;
    NSString *_verifySignature;
    NSString *_sendChallengeTimeout;
    NSString *_lockAuthenticationTimeout;
    NSString *_protocolViolation;
    NSString *_contextViolation;

}
@end

@implementation ConstantErrorMessages

-(id)init
{
    if (self = [super init]) {
        [self initErrorMessages];
        
    }
    return self;
}

-(void)initErrorMessages{
    _disConnect=BLE_DISCONNECT;
    _failedConnection=BLE_FAILED_CONNECTION;
    _certificateTimeout=ECDSA_CERTIFICATE_TIMEOUT;
    _sendCertificateFailure=ECDSA_SEND_CERTIFICATE_FAILURE;
    _requestChallengeTimeout=ECDSA_REQUEST_CHALLENGE_TIMEOUT;
    _sendSignatureTimeout=ECDSA_SEND_SIGNATURE_TIMEOUT;
    _verifySignature=ECDSA_VERIFY_SIGNATURE;
    _sendChallengeTimeout=ECDSA_SEND_CHALLENGE_TIMEOUT;
    _lockAuthenticationTimeout=ECDSA_LOCK_AUTHENTICATED_TIMEOUT;
    _protocolViolation=ECDSA_PROTOCOL_VIOLATION;
    _contextViolation=ECDSA_CONTEXT_VIOLATION;
}



@end
