//
//  BLE_Advertisement.m
//  Spectrum
//
//  Created by Robert on 5/9/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "BLE_Advertisement.h"
#import "BLE_Data.h"

@interface BLE_Advertisement(){
    NSString *_uniqueID;
    NSString *_companyID;
    NSString *_productID;
    NSString *_productType;
    NSString *_protocolVer;
    NSString *_pairingFlag;
    NSString *_notificationIndex;
    NSNumber *_RSSI;
    NSString *_advertisedName;
    BOOL _lockInBLE_Range;
    NSUUID *_UUID_OfPeripheral;
    id <BLEAdvertisementDelegate> _VCdelegate;
}

@end

@implementation BLE_Advertisement

NSNumber *RSSI_Threshhold;




-(id)initWithData:(NSData *)ManufacturerData FriendlyName:(NSString *)AdvertisedName PeripheralIdentifyer:(NSUUID *)peripheralUUID Advertisement_RSSI:(NSNumber *)Adv_RSSI{
    if (self=[super init]) {
        RSSI_Threshhold=[NSNumber numberWithInt:-100];

        _advertisedName=AdvertisedName;
        NSMutableDictionary *dictData=[[NSMutableDictionary alloc]initWithDictionary:[self decodeManufatcurerData:ManufacturerData]];
        _uniqueID=dictData[@"UniqueID"];
        _companyID=dictData[@"CompanyID"];
        _productID=dictData[@"ProductID"];
        _productType=dictData[@"ProductType"];
        _protocolVer=dictData[@"ProtocolVer"];
        _pairingFlag=dictData[@"PairingFlag"];
        _notificationIndex=dictData[@"NotificationIndex"];
        _RSSI=Adv_RSSI;
        _UUID_OfPeripheral=peripheralUUID;
        _VCdelegate=nil;

        if (RSSI_Threshhold<_RSSI) {
            _lockInBLE_Range=YES;
        }else{
            _lockInBLE_Range=NO;
        }
    }
    return self;
}



-(id)initWithDatabaseData:(NSString *)UniqueID ProductType:(NSString *)product_Type NotificationIndex:(NSString *)notifIndx FriendlyName:(NSString *)AdvertisedName PeripheralIdentifyer:(NSUUID *)peripheralUUID {
    if (self=[super init]) {
        RSSI_Threshhold=[NSNumber numberWithInt:-100];
        _advertisedName=AdvertisedName;
        _uniqueID=UniqueID;
        _companyID=@"";
        _productID=@"";
        _productType=product_Type;
        _protocolVer=@"";
        _pairingFlag=@"";
        _notificationIndex=notifIndx;
        _RSSI=nil;
        _UUID_OfPeripheral=peripheralUUID;
        _VCdelegate=nil;
    }
    return self;
}


-(NSMutableDictionary *)decodeManufatcurerData:(NSData *)manufData{
    NSMutableDictionary *retDict=[[NSMutableDictionary alloc]initWithCapacity:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData.count];
  
    BLE_Data *decodeBLE=[[BLE_Data alloc]init];
    //get config data as described in documentation
    NSString *posAndLength4CompanyID=SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"CompanyID"];
    
    //pass binary data and config data to decoding method
    int companyIDFromAdv=[decodeBLE read_uint8_ValueFromBLE_Array:posAndLength4CompanyID NSDataOfArray:manufData];
    [retDict setObject:[NSString stringWithFormat:@"%d",companyIDFromAdv]  forKey:@"CompanyID"];
    int NotificationIndex, ProductType, ProductId, ProtocolVersion, PairingFlag;
    NSString *UniqueID=@"";
    //compare company ID in advertisement to Spectrum Brand's Company ID
    
    
    
    NotificationIndex=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"NotificationIndex"] NSDataOfArray:manufData];
    [retDict setObject:[NSString stringWithFormat:@"%d",NotificationIndex] forKey:@"NotificationIndex"];
    
    ProductType=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"ProductType"] NSDataOfArray:manufData];
    [retDict setObject:[NSString stringWithFormat:@"%d",ProductType] forKey:@"ProductType"];

    ProductId=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"ProductID"] NSDataOfArray:manufData];
    [retDict setObject:[NSString stringWithFormat:@"%d",ProductId] forKey:@"ProductID"];
    
    ProtocolVersion=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"ProtocolVer"] NSDataOfArray:manufData];
    [retDict setObject:[NSString stringWithFormat:@"%d",ProtocolVersion] forKey:@"ProtocolVersion"];
    
    PairingFlag=[decodeBLE read_uint8_ValueFromBLE_Array: SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"PairingFlag"] NSDataOfArray:manufData];
    [retDict setObject:[NSString stringWithFormat:@"%d",PairingFlag] forKey:@"PairingFlag"];
    
    
    
    UniqueID=[decodeBLE read_rawHex_ValueFromBLE_Array: SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"UniqueID"] NSDataOfArray:manufData];
    [retDict setObject:[NSString stringWithFormat:@"%@",UniqueID] forKey:@"UniqueID"];
    
    
    return retDict;

    
}


-(void)getBLE_Advertisement:(id <BLEAdvertisementDelegate>)VC{
    
    _VCdelegate=VC;
    //int cnt=0;
    //while ((SpectrumApplication.manager.manager.isScanning)&&(cnt<6)) {
      //  [NSThread sleepForTimeInterval:1.5f];
       // cnt++;
    //}
    if (_UUID_OfPeripheral) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self,_UUID_OfPeripheral, nil];
            [SpectrumApplication.manager readAdvertisementForPeripheralWithIdentifyer:dict];
    }else if(self.uniqueID){
        //get lock data by advertisement;
         NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self,_uniqueID, nil];
         [SpectrumApplication.manager readAdvertisementForPeripheralWithDeviceID:dict];
    }

    
}

-(void)discoveredAdvertisement:(NSMutableArray *)arOfAdvData{
   //this method is used by BLEManager to return found advertisement we searched for in getBLE_AdvertisementByIdentifier
 
    _advertisedName=arOfAdvData[0];
    NSMutableDictionary *dictData=[[NSMutableDictionary alloc]initWithDictionary:[self decodeManufatcurerData:arOfAdvData[1]]];
    _uniqueID=dictData[@"UniqueID"];
    _companyID=dictData[@"CompanyID"];
    _productID=dictData[@"ProductID"];
    _productType=dictData[@"ProductType"];
    _protocolVer=dictData[@"ProtocolVer"];
    _pairingFlag=dictData[@"PairingFlag"];
    _notificationIndex=dictData[@"NotificationIndex"];
    _UUID_OfPeripheral=arOfAdvData[2];
    _RSSI=arOfAdvData[3];
    if (RSSI_Threshhold<_RSSI) {
        _lockInBLE_Range=YES;
    }else{
        _lockInBLE_Range=NO;
    }
    if (_VCdelegate) {
        [_VCdelegate catchAdvertisement:self];
    }
}
@end
