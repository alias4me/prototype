//
//  BLEPeripheral+ReaderWriter.m
//  SkyLocks
//
//  Created by Manoj Champanerkar on 4/1/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "BLEPeripheral+SpectrumLock.h"
#import "SPConstants.h"
#import "BLE_Data.h"

/* CODE MOVED TO ENCODING/DECODING CLASS... BLE_Data
@implementation NSData (HexString)

// Not efficent
+(id)dataWithHexString:(NSString *)hex
{
    char buf[3];
    buf[2] = '\0';
    NSAssert(0 == [hex length] % 2, @"Hex strings should have an even number of digits (%@)", hex);
    unsigned char *bytes = malloc([hex length]/2);
    unsigned char *bp = bytes;
    for (CFIndex i = 0; i < [hex length]; i += 2) {
        buf[0] = [hex characterAtIndex:i];
        buf[1] = [hex characterAtIndex:i+1];
        char *b2 = NULL;
        *bp++ = strtol(buf, &b2, 16);
        NSAssert(b2 == buf + 2, @"String should be all hex digits: %@ (bad digit around %d)", hex, i);
    }
    
    return [NSData dataWithBytesNoCopy:bytes length:[hex length]/2 freeWhenDone:YES];
}



-(NSString *)hexString
{
    //Returns hexadecimal string of NSData. Empty string if data is empty.
    
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger          dataLength  = [self length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}


-(NSString *)hexToString:(NSString *)hexStringToConvert: (BOOL)isLittleEndian{
    NSMutableString *asciiString= [[NSMutableString alloc] initWithFormat:@""];
    int i = 0;
    while (i < [hexStringToConvert length])
    {
        NSString * hexChar = [hexStringToConvert substringWithRange: NSMakeRange(i, 2)];
        int value = 0;
        sscanf([hexChar cStringUsingEncoding:NSASCIIStringEncoding], "%x", &value);
        if (isLittleEndian) {
            asciiString = [NSString stringWithFormat:@"%c%@", (char)value, asciiString];
        }else{
            [asciiString appendFormat:@"%c", (char)value];
        }
        i+=2;
    }
    //    htonl(asciiString);
    return asciiString;
}

@end

*/

@implementation BLEPeripheral (SpectrumLock)

-(void)doLockUnLock:(bool)lock
{
    if (self.peripheral.state != CBPeripheralStateConnected) {
        //TODO raise an exception here maybe
        DDLogError (@"Cannot lockUnlock without connection");
        return;
    }
    CBCharacteristic *c = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx];
    
    BLE_Data *encData=[[BLE_Data alloc]init];
    
    
    NSMutableData *hexLock, *hexUnlock;
    NSArray *lockCommand=[[NSArray alloc]initWithObjects:@"3",@"1",@"3", nil];
    NSArray *unlockCommand=[[NSArray alloc]initWithObjects:@"3",@"1",@"5", nil];
    
     hexLock=[encData writeIntToBinaryHexFromArrayOfInt:lockCommand];
    
    hexUnlock=[encData writeIntToBinaryHexFromArrayOfInt:unlockCommand];
    
    //    NSData *data = lock ? [NSData dataWithHexString:@"030103"]: [NSData dataWithHexString:@"030105"];
    NSData *data = lock ? hexLock: hexUnlock;
    [self.peripheral writeValue:data forCharacteristic:c type:CBCharacteristicWriteWithResponse];
}


-(NSData *)readAuthentication
{
    if (self.peripheral.state != CBPeripheralStateConnected) {
        //TODO raise an exception here maybe
        DDLogError (@"Cannot read authentication without connection");
        
    }
    CBCharacteristic *c = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
    
   // BLE_Data *encData=[[BLE_Data alloc]init];
    
    
    //NSMutableData *hexAuth;
    //NSArray *lockCommand=[[NSArray alloc]initWithObjects:@"3",@"1",@"3", nil];
    //NSArray *unlockCommand=[[NSArray alloc]initWithObjects:@"3",@"1",@"5", nil];
    
    //hexLock=[encData writeIntToBinaryHexFromArrayOfInt:lockCommand];
    
    //hexUnlock=[encData writeIntToBinaryHexFromArrayOfInt:unlockCommand];
    
    //    NSData *data = lock ? [NSData dataWithHexString:@"030103"]: [NSData dataWithHexString:@"030105"];
    NSData *data =c.value;// [self.peripheral readValueForCharacteristic:c];
    return data;
}

-(void)identify:(NSArray *)dataToWrite Characteristic:(CBCharacteristic*)lockCharacteristic
{
    if (self.peripheral.state != CBPeripheralStateConnected) {
        //TODO raise an exception here maybe
        DDLogError (@"Cannot lockUnlock without connection");
        return;
    }
   // CBCharacteristic *c = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockIdentify];
    
    BLE_Data *encData=[[BLE_Data alloc]init];
    
    
    NSMutableData *hexIdentify;
    //NSArray *lockIdentify=[[NSArray alloc]initWithObjects:@"255", nil];
    
    hexIdentify=[encData writeIntToBinaryHexFromArrayOfInt:dataToWrite];
    
    
    //    NSData *data = lock ? [NSData dataWithHexString:@"030103"]: [NSData dataWithHexString:@"030105"];
    NSData *data = hexIdentify;
    [self.peripheral writeValue:data forCharacteristic:lockCharacteristic type:CBCharacteristicWriteWithResponse];
}


-(void)requestChallenge:(CBCharacteristic*)lockCharacteristic
{
    if (self.peripheral.state != CBPeripheralStateConnected) {
        //TODO raise an exception here maybe
        DDLogError (@"Cannot request challenge without connection");
        return;
    }
    
    //CBCharacteristic *c = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx];
    
    BLE_Data *encData=[[BLE_Data alloc]init];
    
    
    NSMutableData *hexReqChallenge;
    NSArray *reqChallengeCommand=SpectrumApplication.spectrumLockCharacterisitcs.command_TxReqChallenge;
    
    hexReqChallenge=[encData writeIntToBinaryHexFromArrayOfInt:reqChallengeCommand];
    
    
    
    //    NSData *data = lock ? [NSData dataWithHexString:@"030103"]: [NSData dataWithHexString:@"030105"];
    if (hexReqChallenge) {
        //   CBCharacteristic *cAuth = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
        // [c.service.peripheral setNotifyValue:YES forCharacteristic:cAuth];
        NSData *data = hexReqChallenge;
        DDLogDebug(@"about to send write data to uuid '%@'", [lockCharacteristic.UUID UUIDString]);

        DDLogDebug(@"requestChallenge '%@'", data);

        [self.peripheral writeValue:data forCharacteristic:lockCharacteristic type:CBCharacteristicWriteWithResponse];
    }
}



-(void)requestCert
{
    if (self.peripheral.state != CBPeripheralStateConnected) {
        //TODO raise an exception here maybe
        DDLogError (@"request cert without a connection");
        return;
    }
    DDLogDebug(@"entering requestCert");

    CBCharacteristic *c = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx];
    
    BLE_Data *encData=[[BLE_Data alloc]init];
    
    
    NSMutableData *hexReqCert;
    NSArray *reqCertCommand=SpectrumApplication.spectrumLockCharacterisitcs.command_TxReqCert; //[[NSArray alloc]initWithObjects:@"60",@"0", nil];
    
    hexReqCert=[encData writeIntToBinaryHexFromArrayOfInt:reqCertCommand];
    

    
    //    NSData *data = lock ? [NSData dataWithHexString:@"030103"]: [NSData dataWithHexString:@"030105"];
    if (hexReqCert) {
     //   CBCharacteristic *cAuth = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
       // [c.service.peripheral setNotifyValue:YES forCharacteristic:cAuth];
        DDLogError (@"requesting certificate ");
        NSData *data = hexReqCert;
        [self.peripheral writeValue:data forCharacteristic:c type:CBCharacteristicWriteWithResponse];
        DDLogError (@" certificate was requested");
    }
}

-(void)sendSignature:(CBCharacteristic*)lockCharacteristic appSignature:(NSData *)signature
{
    if (self.peripheral.state != CBPeripheralStateConnected) {
        //TODO raise an exception here maybe
        DDLogError (@"send signature without a connection");
        return;
    }

    BLE_Data *encData=[[BLE_Data alloc]init];

    if (!signature) {
        signature=[encData writeIntToBinaryHexFromArrayOfInt:[[NSArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",@"31",@"32", nil]];
    }
   // CBCharacteristic *c = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx];
    
    
    
    NSMutableData *hexSendSignature;
    NSArray *sendSignatureCommand=SpectrumApplication.spectrumLockCharacterisitcs.command_TxSendSignature;
    
    hexSendSignature=[encData writeIntToBinaryHexFromArrayOfInt:sendSignatureCommand];
    
    DDLogDebug(@"about to send write data to uuid '%@'", [lockCharacteristic.UUID UUIDString]);

    
    //    NSData *data = lock ? [NSData dataWithHexString:@"030103"]: [NSData dataWithHexString:@"030105"];
    if (hexSendSignature) {
        //   CBCharacteristic *cAuth = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
        // [c.service.peripheral setNotifyValue:YES forCharacteristic:cAuth];
        NSMutableData *data = hexSendSignature;
        [data appendData:signature];
        DDLogDebug(@"sendSignature '%@'", data);
        [self.peripheral writeValue:data forCharacteristic:lockCharacteristic type:CBCharacteristicWriteWithResponse];
    }
}

//command_TxSetFriendlyName

-(void)setFriendlyName:(CBCharacteristic*)lockCharacteristic FriendlyName:(NSString *)friendlyName
{
    if (self.peripheral.state != CBPeripheralStateConnected) {
        //TODO raise an exception here maybe
        DDLogError (@"Cannot lockUnlock without connection");
        return;
    }
    NSString *len=@"";
    BLE_Data *encData=[[BLE_Data alloc]init];
    NSData *dataFriendlyName;
    if (friendlyName.length==0) {
        dataFriendlyName=[encData writeIntToBinaryHexFromArrayOfInt:[[NSArray alloc]initWithObjects:@"82",@"111",@"98",@"101",@"114",@"105",@"80",@"104",@"111",@"110",@"101", nil]];
        len=[encData writeIntToBinaryHexFromArrayOfInt:[[NSArray alloc]initWithObjects:@"11", nil]];
    }
    // CBCharacteristic *c = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx];
    
    
    
    NSMutableData *hexSetFriendlyName;
    NSArray *setFriendlyNameCommand=SpectrumApplication.spectrumLockCharacterisitcs.command_TxSetFriendlyName;
    
    hexSetFriendlyName=[encData writeIntToBinaryHexFromArrayOfInt:setFriendlyNameCommand];
    
    DDLogDebug(@"about to send write data to uuid '%@'", [lockCharacteristic.UUID UUIDString]);
    
    
    //    NSData *data = lock ? [NSData dataWithHexString:@"030103"]: [NSData dataWithHexString:@"030105"];
    if (hexSetFriendlyName) {
        //   CBCharacteristic *cAuth = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
        // [c.service.peripheral setNotifyValue:YES forCharacteristic:cAuth];
        NSMutableData *data = hexSetFriendlyName;
        [data appendData:dataFriendlyName];
        DDLogDebug(@"sendChallenge '%@'", data);
        [self.peripheral writeValue:data forCharacteristic:lockCharacteristic type:CBCharacteristicWriteWithResponse];
    }
}

-(void)sendChallenge:(CBCharacteristic*)lockCharacteristic appChallenge:(NSData *)challenge
{
    if (self.peripheral.state != CBPeripheralStateConnected) {
        //TODO raise an exception here maybe
        DDLogError (@"Cannot lockUnlock without connection");
        return;
    }
    
    BLE_Data *encData=[[BLE_Data alloc]init];
    
    if (!challenge) {
        challenge=[encData writeIntToBinaryHexFromArrayOfInt:[[NSArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",@"31",@"32", nil]];
    }
    // CBCharacteristic *c = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx];
    
    
    
    NSMutableData *hexSendchallenge;
    NSArray *sendchallengeCommand=SpectrumApplication.spectrumLockCharacterisitcs.command_TxSendChallenge;
    
    hexSendchallenge=[encData writeIntToBinaryHexFromArrayOfInt:sendchallengeCommand];
    
    DDLogDebug(@"about to send write data to uuid '%@'", [lockCharacteristic.UUID UUIDString]);
    
    
    //    NSData *data = lock ? [NSData dataWithHexString:@"030103"]: [NSData dataWithHexString:@"030105"];
    if (hexSendchallenge) {
        //   CBCharacteristic *cAuth = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
        // [c.service.peripheral setNotifyValue:YES forCharacteristic:cAuth];
        NSMutableData *data = hexSendchallenge;
        [data appendData:challenge];
        DDLogDebug(@"sendChallenge '%@'", data);
        [self.peripheral writeValue:data forCharacteristic:lockCharacteristic type:CBCharacteristicWriteWithResponse];
    }
}



-(void)confirmAuthentication:(CBCharacteristic*)lockCharacteristic
{
    if (self.peripheral.state != CBPeripheralStateConnected) {
        //TODO raise an exception here maybe
        DDLogError (@"Cannot lockUnlock without connection");
        return;
    }
    
    BLE_Data *encData=[[BLE_Data alloc]init];
    
    // CBCharacteristic *c = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx];
    
    
    
    NSMutableData *hexConfirmAuthentication;
    NSArray *ConfirmAuthenticationCommand=SpectrumApplication.spectrumLockCharacterisitcs.command_TxConfirmAuthentication;
    
    hexConfirmAuthentication=[encData writeIntToBinaryHexFromArrayOfInt:ConfirmAuthenticationCommand];
    
    DDLogDebug(@"about to send write data to uuid '%@'", [lockCharacteristic.UUID UUIDString]);
    
    
    //    NSData *data = lock ? [NSData dataWithHexString:@"030103"]: [NSData dataWithHexString:@"030105"];
    if (hexConfirmAuthentication) {
        //   CBCharacteristic *cAuth = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockAuthentication];
        // [c.service.peripheral setNotifyValue:YES forCharacteristic:cAuth];
        NSMutableData *data = hexConfirmAuthentication;

        DDLogDebug(@"sendSignature '%@'", data);
        [self.peripheral writeValue:data forCharacteristic:lockCharacteristic type:CBCharacteristicWriteWithResponse];
    }
}

-(void)command_TX:(NSArray *)dataToWrite DataTypeForEncoding:(int)dataType Characteristic:(CBCharacteristic*)lockCharacteristic{
    if (self.peripheral.state != CBPeripheralStateConnected) {
        //TODO raise an exception here maybe
        DDLogError (@"Cannot lockUnlock without connection");
        return;
    }
  //  CBCharacteristic *c = [self characteristicForUUID:SpectrumApplication.spectrumLockCharacterisitcs.lockSystemCommandTx];
    
    BLE_Data *encData=[[BLE_Data alloc]init];
    
    
    NSMutableData *hexData=[[NSMutableData alloc]init];
//    NSArray *reqCertCommand=[[NSArray alloc]initWithObjects:@"60",@"0", nil];
    switch (dataType) {
        case 0:
            //int
            hexData=[encData writeIntToBinaryHexFromArrayOfInt:dataToWrite];

            break;
        case 1:
         //ASCII
            //hexData=[[NSMutableData alloc]init];
            for (NSString *s in dataToWrite) {
                NSData *cmdData=[[s dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES] mutableCopy];
                [hexData appendData:cmdData];
            }
            //hexData=[[asciiString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES] mutableCopy];
            break;
        default:
            break;
    }
    
    NSLog(@"Commant_Tx hex to write=", hexData);
    
    //    NSData *data = lock ? [NSData dataWithHexString:@"030103"]: [NSData dataWithHexString:@"030105"];
    NSData *data = hexData;
    [self.peripheral writeValue:data forCharacteristic:lockCharacteristic type:CBCharacteristicWriteWithResponse];

    
}

@end
