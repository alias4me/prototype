//
//  ErrorMessages.h
//  Spectrum
//
//  Created by Robert on 7/4/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConstantErrorMessages : NSObject

#define  BLE_FAILED_CONNECTION  @"Your connection to the lock failed. please verify the lock is not already paired with your phone. Go to iOS settings --> bluetooth and click on the circled i next to the lock name from the list then click 'Forget This Device'."
#define  BLE_DISCONNECT  @"You have suddenly lost connection to the lock. Please make sure you are within BLE range of the lock."

#define  ECDSA_CERTIFICATE_TIMEOUT  @"The lock failed to Authenticate in time. Please remove the pair by going to iOS settings --> bluetooth and click on the circled i next to the lock name from the list then click 'Forget This Device'. Once the pair is removed you can try again. Error Code 07"

#define  ECDSA_SEND_CERTIFICATE_FAILURE  @"The lock failed to Authenticate in time. Please remove the pair by going to iOS settings --> bluetooth and click on the circled i next to the lock name from the list then click 'Forget This Device'. Once the pair is removed you can try again. Error Code 08"

#define  ECDSA_REQUEST_CHALLENGE_TIMEOUT  @"The lock failed to Authenticate in time. Please remove the pair by going to iOS settings --> bluetooth and click on the circled i next to the lock name from the list then click 'Forget This Device'. Once the pair is removed you can try again. Error Code 09"

#define  ECDSA_SEND_SIGNATURE_TIMEOUT  @"The lock failed to Authenticate in time. Please remove the pair by going to iOS settings --> bluetooth and click on the circled i next to the lock name from the list then click 'Forget This Device'. Once the pair is removed you can try again. Error Code 10"

#define  ECDSA_VERIFY_SIGNATURE  @"The lock failed to Authenticate in time. Please remove the pair by going to iOS settings --> bluetooth and click on the circled i next to the lock name from the list then click 'Forget This Device'. Once the pair is removed you can try again. Error Code 11"

#define  ECDSA_SEND_CHALLENGE_TIMEOUT  @"The lock failed to Authenticate in time. Please remove the pair by going to iOS settings --> bluetooth and click on the circled i next to the lock name from the list then click 'Forget This Device'. Once the pair is removed you can try again. Error Code 12"

#define  ECDSA_LOCK_AUTHENTICATED_TIMEOUT  @"The lock failed to Authenticate in time. Please remove the pair by going to iOS settings --> bluetooth and click on the circled i next to the lock name from the list then click 'Forget This Device'. Once the pair is removed you can try again. Error Code 13"

#define  ECDSA_PROTOCOL_VIOLATION  @"The lock failed to Authenticate in time. Please remove the pair by going to iOS settings --> bluetooth and click on the circled i next to the lock name from the list then click 'Forget This Device'. Once the pair is removed you can try again. Error Code 15"

#define  ECDSA_CONTEXT_VIOLATION  @"The lock failed to Authenticate in time. Please remove the pair by going to iOS settings --> bluetooth and click on the circled i next to the lock name from the list then click 'Forget This Device'. Once the pair is removed you can try again. Error Code 16"



@property(strong, nonatomic)NSString *failedConnection;
@property(strong, nonatomic)NSString *disConnect;
@property(strong, nonatomic)NSString *certificateTimeout;
@property(strong, nonatomic)NSString *sendCertificateFailure;
@property(strong, nonatomic)NSString *requestChallengeTimeout;
@property(strong, nonatomic)NSString *sendSignatureTimeout;
@property(strong, nonatomic)NSString *verifySignature;
@property(strong, nonatomic)NSString *sendChallengeTimeout;
@property(strong, nonatomic)NSString *lockAuthenticationTimeout;
@property(strong, nonatomic)NSString *protocolViolation;
@property(strong, nonatomic)NSString *contextViolation;

@end
