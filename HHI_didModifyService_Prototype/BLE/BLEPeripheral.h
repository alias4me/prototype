//
//  BLELock.h
//  SkyLocks
//
//  Created by Manoj Champanerkar on 3/21/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//


#import <Foundation/Foundation.h>

#import <CoreBluetooth/CBPeripheral.h>
#import <CoreBluetooth/CBService.h>
#import <CoreBluetooth/CBUUID.h>
#import <CoreBluetooth/CBCharacteristic.h>
#import "BLE_Advertisement.h"

@class BLEPeripheral; // forward declaration

@protocol BLEPeripheralDelegate

-(void)didConnect:(BLEPeripheral *)peripheral;
-(void)didDisConnect:(BLEPeripheral *)peripheral;
-(void)didFailToConnectToPeripheral:(BLEPeripheral *)peripheral error:(NSError *)error;
-(void)didDiscoverServices:(BLEPeripheral *)peripheral Service:(CBService *)service;

@optional
-(void)didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)c;
-(void)catchCharacteristicValue:(CBCharacteristic *)c peripheral:(BLEPeripheral *)periph;
-(void)didDiscoverCharacteristic:(CBCharacteristic *)ch Peripheral:(CBPeripheral *)peripheral;
-(void)characteristicDidWriteValue:(CBCharacteristic *)ch Peripheral:(BLEPeripheral *)peripheral;
-(void)characteristicIsReadyToWrite:(BLEPeripheral *)peripheral;
@end





@interface BLEPeripheral : NSObject


@property (readonly, nonatomic) bool isLock;
@property (readonly, nonatomic) NSString *name;
@property (readonly, nonatomic) NSString *UUIDString;
@property (readwrite, nonatomic) CBPeripheral *peripheral; //TODO make this private
@property (readwrite, nonatomic) NSDictionary<NSString *,id> *advertisementData;
@property (readwrite, nonatomic) NSNumber *rssi;
@property(assign,nonatomic)BOOL forcePairingByReadingCharacteristicValue;
@property (readonly, nonatomic) NSArray *services;

@property (assign,nonatomic)BOOL authenticationProcess;
@property (nonatomic) id<BLEPeripheralDelegate> delegate;


//@property(readonly,nonatomic)BOOL isGettingSystemInfo;

-(id) initWithPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI;


-(void)discoverServices;
-(void)discoverServicesForUUIDs:(NSArray<CBUUID *>*)UUID_Array;
//these should be private !!!
-(void)connectToPeripheral:(CBPeripheral *)peripheral;
-(void)disConnect;

-(CBCharacteristic *)characteristicForUUID:(CBUUID *)uuid;
-(NSMutableDictionary *)getLockSystemInfo;
-(NSMutableDictionary <CBUUID*,NSData*>*)getLockSystemSettings;
-(NSMutableDictionary <CBUUID*,NSData*>*)getLockSystemState;
-(BLE_Advertisement *)getAdvData;

@end

@interface NSData(HexString)
@end

