//
//  BLEManager.m
//  SkyLocks
//
//  Created by Manoj Champanerkar on 3/21/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "BLEManager.h"
#import "SPConstants.h"
#import "BLEPeripheral+SpectrumLock.h"
#import "BLE_Data.h"
#import "BLE_Advertisement.h"

@interface BLEManager () <CBCentralManagerDelegate>
{
    CBCentralManager *_manager;
    NSMutableDictionary <NSUUID *, BLEPeripheral*> *_peripherals;
    BLEPeripheral *_pendingConnectPeripheral;
  NSMutableArray <NSUUID*>*_peripheralsAlreadyInDatabase;
}

@property (readonly, nonatomic) CBManagerState state;
@end

BOOL searchingAdvertisementsByDeviceID=NO;
BOOL searchingAdvertisementsByIdentifiers=NO;
BOOL searchingAdvertisementsForNewLocks=NO;
BOOL searchingLocksForConnection=NO;
BOOL authenticationProcess=NO;

NSMutableDictionary <NSUUID*,BLE_Advertisement*> *aUUIDs_UsedToSearchWithAdvertisement;
NSMutableDictionary <NSUUID*,BLE_Advertisement*> *aDeviceID_UsedToSearchWithAdvertisement;
//BLE_Advertisement *advertisementClassRequestingData;

NSTimer *scanTimer;

@implementation BLEManager



-(id)init
{
    if (self = [super init])
    {
        _manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
        _peripherals = [[NSMutableDictionary alloc] init];
        _discoveredPeripherals = [[NSMutableArray alloc] init];
        _peripheralsAlreadyInDatabase=[[NSMutableArray<NSUUID*> alloc]init];
        aUUIDs_UsedToSearchWithAdvertisement=[[NSMutableDictionary <NSUUID*,BLE_Advertisement*> alloc]init];
        aDeviceID_UsedToSearchWithAdvertisement=[[NSMutableDictionary <NSUUID*,BLE_Advertisement*> alloc]init];
        //searchingAdvertisementsByDeviceID
    }
    return self;
}

-(CBManagerState) state
{
    return _manager.state;
}

-(BLEPeripheral *)peripheralWithUUID:(NSUUID *)uuid
{
    return _peripherals[uuid];
}


-(void)addPeripheralToExclude:(NSUUID*)uuidToExclude{
    for (int i=0; i<_discoveredPeripherals.count; i++) {
        BLEPeripheral *p=_discoveredPeripherals[i];
        if ([p.peripheral.identifier isEqual:uuidToExclude]) {
            [_discoveredPeripherals removeObjectAtIndex:i];
        }
    }
    
    [_peripherals removeObjectForKey:uuidToExclude];

    [_peripheralsAlreadyInDatabase addObject:uuidToExclude];
}

-(void)removePeriphrtalToExclure:(NSUUID*)uuidToStopExcluding{
    
    [_peripheralsAlreadyInDatabase removeObject:uuidToStopExcluding];
}

-(void)setPeriphersToExcludeFromScans:(NSMutableArray <NSUUID*>*)PeripheralIdentifiersToExclud{
    _peripheralsAlreadyInDatabase=PeripheralIdentifiersToExclud;
}


-(void)scanForNewLocks{
    searchingAdvertisementsForNewLocks=YES;
    [self scanAll];
}

-(void)checkForOtherScans{
    if ((searchingAdvertisementsForNewLocks==NO)&&(searchingLocksForConnection==NO)&&(searchingAdvertisementsByIdentifiers==NO)) {
        [self stopScan];
    }

}

-(void)stopScanningForNewLocks{
    searchingAdvertisementsForNewLocks=NO;
    [self checkForOtherScans];
}

-(void)scanAll
{
    if (@available(iOS 10.0, *)) {
        if (_manager.state == CBManagerStatePoweredOn) {
            if (_manager.isScanning) {
                DDLogDebug(@"scan in progress.. scanAll aborted");
                return;
            };
            DDLogInfo (@"scanning started...");
            [_discoveredPeripherals removeAllObjects];
            [_peripherals removeAllObjects];
            // [_peripherals ];
            
            
            [_manager scanForPeripheralsWithServices:nil options:nil];
            
            
            [SpectrumApplication
             postNotificationWithName:BLEManagerStateChangeNotification
             sender: SpectrumApplication.manager
             object:@""];
            //TODO later set timeer to stop scan to avoid batter
        }
    } else {
        // Fallback on earlier versions
    }
    
    if (@available(iOS 10.0, *)) {
        if (_manager.state == CBManagerStatePoweredOff) {
            //TODO show dialog to switch on blue tooth
            
        }
    } else {
        // Fallback on earlier versions
    }
    if (@available(iOS 10.0, *)) {
        if (_manager.state == CBManagerStateUnauthorized) {
            //TODO show dialog for permission !!
        }
    } else {
        // Fallback on earlier versions
    }
    
}

-(void)stopScan
{
    if (@available(iOS 10.0, *)) {
        if (_manager.state == CBManagerStatePoweredOn) {
            if (!_manager.isScanning) {
                DDLogDebug(@"scan already stopped.. stopScan aborted");
                return;
            };
            DDLogInfo (@"Stopping scan...");
            [_manager stopScan];
            [SpectrumApplication
             postNotificationWithName:BLEManagerStateChangeNotification
             sender: SpectrumApplication.manager
             object:@""];
        }
    } else {
        // Fallback on earlier versions
    }
    
}

/*
 -(void)connectToPeripheralByNumber:(int)peripheralNumb
 {
 BLEPeripheral *peripheral=_manager.
 // stop Scan if scanning
 //if (_manager.isScanning) [self stopScan];
 
 if (_pendingConnectPeripheral == peripheral) {
 DDLogInfo (@"Already connecting to %@(%@). will keep trying", peripheral.name, peripheral.UUIDString);
 return;
 }
 if (_activePeripheral == peripheral) {
 DDLogInfo (@"Already connected to %@(%@). nothing to do..", peripheral.name, peripheral.UUIDString);
 return;
 }
 
 if (_pendingConnectPeripheral != nil) {
 DDLogInfo (@"aborting connetion attempt to: %@(%@)", _pendingConnectPeripheral.name, _pendingConnectPeripheral.UUIDString);
 [_manager cancelPeripheralConnection:_pendingConnectPeripheral.peripheral];
 
 }
 //DDLogInfo (@"connecting to %@(%@)...", peripheral.name, peripheral.UUIDString);
 _pendingConnectPeripheral = peripheral;
 [_manager connectPeripheral:peripheral.peripheral options:nil];
 }
 */

-(void)connectToPeripheral:(BLEPeripheral *)peripheral SetAuthentificationToNotify:(BOOL)setAuthenticationToNotify
{
    // stop Scan if scanning
    if (_manager.isScanning) [self stopScan];
    
    if (_pendingConnectPeripheral == peripheral) {
        DDLogInfo (@"Already connecting to %@(%@). will keep trying", peripheral.name, peripheral.UUIDString);
        return;
    }
    if (_activePeripheral == peripheral) {
        DDLogInfo (@"Already connected to %@(%@). nothing to do..", peripheral.name, peripheral.UUIDString);
        return;
    }
    
    if (_pendingConnectPeripheral != nil) {
        DDLogInfo (@"aborting connetion attempt to: %@(%@)", _pendingConnectPeripheral.name, _pendingConnectPeripheral.UUIDString);
        [_manager cancelPeripheralConnection:_pendingConnectPeripheral.peripheral];
        
    }
    //DDLogInfo (@"connecting to %@(%@)...", peripheral.name, peripheral.UUIDString);
   
    peripheral.authenticationProcess=setAuthenticationToNotify;
    _pendingConnectPeripheral = peripheral;
    
    //TODO: To Robert, Sometimes app crashes here as peripheral.peripheral is nil ,*** Assertion failure in -[CBCentralManager connectPeripheral:options:], /BuildRoot/Library/Caches/com.apple.xbs/Sources/MobileBluetoothFramework/MobileBluetooth-110.99.12/CoreBluetooth/CoreBluetooth/CBCentralManager.m:293 2018-07-13 18:21:49.257889+0530 Spectrum-Integration[1256:351608] *** Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'Invalid parameter not satisfying: peripheral != nil' First throw call stack: libc++abi.dylib: terminating with uncaught exception of type NSException
    [_manager connectPeripheral:peripheral.peripheral options:nil];
    
}
//
-(void)scanDone:(NSTimer*)timer {
    if(_manager.isScanning) {
        //[_manager stopScan];
        [self checkForOtherScans];
    }
    searchingAdvertisementsByIdentifiers=NO;

    [aUUIDs_UsedToSearchWithAdvertisement removeAllObjects];
    [timer invalidate];
    
    
}


-(void)disconnect: (BLEPeripheral *)peripheral;
{
    if (peripheral.peripheral.state == CBPeripheralStateConnected)
        [_manager cancelPeripheralConnection:peripheral.peripheral];
}







-(CBPeripheral*)getPeripheralByIdentifier:(NSUUID *)PeripheralIdentifier{
    _manager.delegate=self;
    
    NSMutableArray <NSUUID *>*arrPeriphIdentifier=[[NSMutableArray alloc]initWithObjects:PeripheralIdentifier, nil];
    NSArray <CBPeripheral *>* arrPer=[_manager retrievePeripheralsWithIdentifiers:arrPeriphIdentifier];
    if (arrPer.count>0) {
        return arrPer[0];
    }else{
        return nil;
    }
}

-(void)readAdvertisementForPeripheralWithIdentifyer:(NSMutableDictionary <NSUUID*, BLE_Advertisement*>*)PeripheralIdentifierAndAdvClass
{
    
    searchingAdvertisementsByIdentifiers=YES;
    for (id key in PeripheralIdentifierAndAdvClass) {
        [aUUIDs_UsedToSearchWithAdvertisement setObject:PeripheralIdentifierAndAdvClass[key] forKey:key];

    }

    _manager.delegate=self;

    if (!scanTimer) {
        [scanTimer invalidate];
        scanTimer= [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(scanDone:) userInfo:nil repeats:NO];
    }else{
        scanTimer= [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(scanDone:) userInfo:nil repeats:NO];
    }

    [self scanAll];
    
}



-(void)readAdvertisementForPeripheralWithDeviceID:(NSMutableDictionary <NSUUID*, BLE_Advertisement*>*)PeripheralDeviceIDAndAdvClass
{
    
    searchingAdvertisementsByDeviceID=YES;
    for (id key in PeripheralDeviceIDAndAdvClass) {
        [aDeviceID_UsedToSearchWithAdvertisement setObject:PeripheralDeviceIDAndAdvClass[key] forKey:key];
        
    }
    
    _manager.delegate=self;
    
    if (!scanTimer) {
        [scanTimer invalidate];
        scanTimer= [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(scanDone:) userInfo:nil repeats:NO];
    }else{
        scanTimer= [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(scanDone:) userInfo:nil repeats:NO];
    }
    
    [self scanAll];
    
}


-(BOOL)isSpectrumAdvertisement:(NSDictionary<NSString *,id>*)advertisementData PEriph:(CBPeripheral *)p{
   //Method filters BLE devices out by looking for Spectrum Brands Company ID in CBAdvertisementDataManufacturerDataKey
    BOOL retIsSpectrumAdv=NO;
    @try{
        
        NSData *dataBytes = [advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];
        // if ([[p.name substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"SB"]) {
        
        //Should the configuration info to read and write to BLE be encrypted?
        
        BLE_Data *decodeBLE=[[BLE_Data alloc]init];
        //get config data as described in documentation
        NSString *posAndLength4CompanyID=SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"CompanyID"];
       // NSString *PairingFlag=SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"PairingFlag"];
        //pass binary data and config data to decoding method
        int companyIDFromAdv=[decodeBLE read_uint8_ValueFromBLE_Array:posAndLength4CompanyID NSDataOfArray:dataBytes];
        int NotificationIndex, ProductId, ProtocolVersion, PairingFlag;
        NSString *UniqueID=@"";

        //compare company ID in advertisement to Spectrum Brand's Company ID
        if (companyIDFromAdv==SpectrumBrandsCompanyID){
         
            if (![_peripheralsAlreadyInDatabase containsObject:p.identifier]) {
                
                NotificationIndex=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"NotificationIndex"] NSDataOfArray:dataBytes];
            
                ProductId=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"ProductID"] NSDataOfArray:dataBytes];
                ProtocolVersion=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"ProtocolVer"] NSDataOfArray:dataBytes];
                PairingFlag=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"PairingFlag"] NSDataOfArray:dataBytes];
                
                UniqueID=[decodeBLE read_rawHex_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"UniqueID"] NSDataOfArray:dataBytes];
                //[decodeBLE byteArray:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"UniqueID"] NSDataOfArray:dataBytes];
                if (PairingFlag==1) {
                    retIsSpectrumAdv=YES;
                }
                
            }
        }
        
    }@catch(NSException *ex){
        DDLogInfo(@"Error in reading advertisement %@",ex.reason);
        return  retIsSpectrumAdv;
    }
    
    return retIsSpectrumAdv;
}



-(NSString *)getDeviceID:(NSDictionary<NSString *,id>*)advertisementData{
    NSString *UniqueID=@"";
    @try{
        
        NSData *dataBytes = [advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];
        // if ([[p.name substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"SB"]) {
        
        //Should the configuration info to read and write to BLE be encrypted?
        
        BLE_Data *decodeBLE=[[BLE_Data alloc]init];
        //get config data as described in documentation
        NSString *posAndLength4CompanyID=SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"CompanyID"];
        // NSString *PairingFlag=SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"PairingFlag"];
        //pass binary data and config data to decoding method
        int companyIDFromAdv=[decodeBLE read_uint8_ValueFromBLE_Array:posAndLength4CompanyID NSDataOfArray:dataBytes];
        int NotificationIndex, ProductId, ProtocolVersion, PairingFlag;
        
        //compare company ID in advertisement to Spectrum Brand's Company ID
        if (companyIDFromAdv==SpectrumBrandsCompanyID){
            
            
                NotificationIndex=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"NotificationIndex"] NSDataOfArray:dataBytes];
                
                ProductId=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"ProductID"] NSDataOfArray:dataBytes];
                ProtocolVersion=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"ProtocolVer"] NSDataOfArray:dataBytes];
                PairingFlag=[decodeBLE read_uint8_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"PairingFlag"] NSDataOfArray:dataBytes];
                
                UniqueID=[decodeBLE read_rawHex_ValueFromBLE_Array:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"UniqueID"] NSDataOfArray:dataBytes];
                //[decodeBLE byteArray:SpectrumApplication.spectrumLockCharacterisitcs.configForAdvertisementData[@"UniqueID"] NSDataOfArray:dataBytes];
            
                
            
        }
        
    }@catch(NSException *ex){
        DDLogInfo(@"Error in reading advertisement %@",ex.reason);
        return nil;
    }
    return UniqueID;
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSString *stateErrorString = @"";
    
    switch (central.state)
    {
        case CBManagerStateUnsupported:
            stateErrorString = @"Bluetooth Low Energy is unsupported.";
            break;
        case CBManagerStateUnauthorized:
            stateErrorString = @"This app is not authorized to use Bluetooth Low Energy.";
            break;
        case CBManagerStatePoweredOff:
            stateErrorString = @"Bluetooth on this device is currently powered off.";
            break;
        case CBManagerStateResetting:
            stateErrorString = @"The Bluetooth Low Energy Manager is resetting; a state update is pending.";
            break;
        case CBManagerStatePoweredOn:
            stateErrorString = @"";
            break;
        case CBManagerStateUnknown:
            stateErrorString = @"The state of the Bluetooth Low Energy Manager is unknown.";
            break;
        default:
            stateErrorString = @"The state of the Bluetooth Low Energy Manager is unknown.";
    }
    DDLogInfo (@"Central Manager state changed - %@", stateErrorString);
    
    [SpectrumApplication
     postNotificationWithName:BLEManagerStateChangeNotification
     sender: SpectrumApplication.manager
     object:stateErrorString];
    
}


-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI {
    
    NSData   *mfgData   = [advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];
    if (mfgData){
        if (searchingAdvertisementsByDeviceID==YES) {
            DDLogInfo (@"discovered peripheral %@ (%@) Advertisement packet pair count=%lu", peripheral.name, peripheral.identifier.UUIDString,(unsigned long)advertisementData.count);
            
            
            DDLogDebug(@"discovered peripheral %@ (%@) Advertisement packet pair count=%lu", peripheral.name, peripheral.identifier.UUIDString,(unsigned long)advertisementData.count);
            //Just getting advertisement data to update Notification Index
            NSString *lockID=[self getDeviceID:advertisementData];
            
            if (aDeviceID_UsedToSearchWithAdvertisement[lockID]) {
                NSMutableArray *arrAdv=[[NSMutableArray alloc]initWithObjects:[advertisementData objectForKey:CBAdvertisementDataLocalNameKey],mfgData,peripheral.identifier, RSSI, nil] ;
                BLE_Advertisement *advClass=aDeviceID_UsedToSearchWithAdvertisement[lockID];
                
                [advClass discoveredAdvertisement:arrAdv];
                [aDeviceID_UsedToSearchWithAdvertisement removeObjectForKey:lockID];
                
                if (aDeviceID_UsedToSearchWithAdvertisement.count==0) {
                    searchingAdvertisementsByDeviceID=NO;
                    [self scanDone:scanTimer];
                }
            }
        }
        if (searchingAdvertisementsByIdentifiers==YES) {
            DDLogInfo (@"discovered peripheral %@ (%@) Advertisement packet pair count=%lu", peripheral.name, peripheral.identifier.UUIDString,(unsigned long)advertisementData.count);


            DDLogDebug(@"discovered peripheral %@ (%@) Advertisement packet pair count=%lu", peripheral.name, peripheral.identifier.UUIDString,(unsigned long)advertisementData.count);
            //Just getting advertisement data to update Notification Index
            if (aUUIDs_UsedToSearchWithAdvertisement[peripheral.identifier]) {
                NSMutableArray *arrAdv=[[NSMutableArray alloc]initWithObjects:[advertisementData objectForKey:CBAdvertisementDataLocalNameKey],mfgData,peripheral.identifier, RSSI, nil] ;
                BLE_Advertisement *advClass=aUUIDs_UsedToSearchWithAdvertisement[peripheral.identifier];
                
                [advClass discoveredAdvertisement:arrAdv];
                [aUUIDs_UsedToSearchWithAdvertisement removeObjectForKey:peripheral.identifier];
               
                if (aUUIDs_UsedToSearchWithAdvertisement.count==0) {
                    searchingAdvertisementsByIdentifiers=NO;
                    [self scanDone:scanTimer];
                }
            }
        }

        if(searchingAdvertisementsForNewLocks==YES){
            //getting all Spectrum locks not already discovered
            DDLogInfo (@"discovered peripheral %@ (%@) Advertisement packet pair count=%lu", peripheral.name, peripheral.identifier.UUIDString,(unsigned long)advertisementData.count);
            
            if ([self isSpectrumAdvertisement:advertisementData PEriph:peripheral]) {
                BLEPeripheral *p = _peripherals[peripheral.identifier];
                
                DDLogDebug(@"discovered peripheral %@ (%@) Advertisement packet pair count=%lu", peripheral.name, peripheral.identifier.UUIDString,(unsigned long)advertisementData.count);
                if (!p) {
                    p = [[BLEPeripheral alloc] initWithPeripheral:peripheral advertisementData:advertisementData RSSI:RSSI];
                    _peripherals[peripheral.identifier] = p;
                    [_discoveredPeripherals addObject:p];
                    
                    [SpectrumApplication postNotificationWithName:BLEPeripheralFoundNotification sender:SpectrumApplication.manager object:nil];
                }
                else {
                    p.peripheral = peripheral;
                    p.rssi = RSSI;
                    [SpectrumApplication postNotificationWithName:BLEPeripheralRefreshNotification sender:SpectrumApplication.manager object:p];
                }
            }
            
        }
    }
}

-(void)registerPeripheral:(BLEPeripheral *)peripheralToRegister{
    if (!_peripherals[peripheralToRegister.peripheral.identifier]) {
        _peripherals[peripheralToRegister.peripheral.identifier]=peripheralToRegister;
    }
}

-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    
    BLEPeripheral *p = _peripherals [peripheral.identifier];
    DDLogInfo (@"Disconnected peripheral: %@ (%@), error: %@", peripheral.name, peripheral.identifier.UUIDString, error);
    
    if (p) {
        
        [p disConnect];
        
        if (_activePeripheral == p)
            _activePeripheral = nil;
        
    }
    
}

-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
   //Lock may be already paired while authenticating
    
    DDLogError (@"Failed to connect to peripheral:%@(%@)", peripheral.name, peripheral.identifier.UUIDString);
    _activePeripheral = nil;
    
    [_pendingConnectPeripheral.delegate didFailToConnectToPeripheral:_pendingConnectPeripheral error:error];
    //Returns Nil if failed
    //[SKYApp postNotificationWithName:BLEPeripheralConnectErrorNotification object:peripheral.identifier];
    _pendingConnectPeripheral  = nil;
    
}

-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    DDLogInfo (@"didConnectToPeripheral: %@(%@), pendingConnectedPeripheral: %@", peripheral.name, peripheral.identifier.UUIDString, _pendingConnectPeripheral.name);
    if (_pendingConnectPeripheral.peripheral.identifier == peripheral.identifier) {
        //[_pendingConnectPeripheral connectToPeripheral:peripheral];
        _activePeripheral = _pendingConnectPeripheral;
        [_activePeripheral connectToPeripheral:peripheral];
       
        
        _pendingConnectPeripheral =nil;
    }
}

@end
