//
//  SpectrumLockCharacteristics.h
//  SkyLocks
//
//  Created by Manoj Champanerkar on 4/1/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import <Foundation/Foundation.h>


#define UUID_LOCK_SYSTEM @"4D050010-766C-42C4-8944-42BC98FC2D09"
#define UUID_SYSTEM_INFO @"4D050011-766C-42C4-8944-42BC98FC2D09"
#define UUID_SYSTEM_STATE @"4D050012-766C-42C4-8944-42BC98FC2D09"
#define UUID_SYSTEM_SETTINGS @"4D050013-766C-42C4-8944-42BC98FC2D09"
#define UUID_ACCESS_CODES @"4D050014-766C-42C4-8944-42BC98FC2D09"
#define UUID_EVENT_LOG @"4D050015-766C-42C4-8944-42BC98FC2D09"
#define UUID_TIME_REGION @"4D050016-766C-42C4-8944-42BC98FC2D09"

#define UUID_LockSystem_CommandTx @"4D050017-766C-42C4-8944-42BC98FC2D09"


#define UUID_LOCK_ACTIVATION  @"4D050080-766C-42C4-8944-42BC98FC2D09"
#define UUID_IDENTIFY  @"4D050081-766C-42C4-8944-42BC98FC2D09"
#define UUID_AUTHENTICATION  @"4D050082-766C-42C4-8944-42BC98FC2D09"
#define UUID_COMMISSIONING_INFO   @"4D050083-766C-42C4-8944-42BC98FC2D09"

#define  UUID_BLE_SYSTEM  @"4D050090-766C-42C4-8944-42BC98FC2D09"
#define  UUID_BONDED_MOBILE_DEVICE_LIST  @"4D050091-766C-42C4-8944-42BC98FC2D09"
#define  UUID_ADVERTISE_SETTINGS  @"4D050092-766C-42C4-8944-42BC98FC2D09"
#define  UUID_CONNECTION_SETTINGS  @"4D050093-766C-42C4-8944-42BC98FC2D09"
#define  UUID_TX_POWER  @"4D050094-766C-42C4-8944-42BC98FC2D09"

#define UUID_BLEOTA  @"4D0500A0-766C-42C4-8944-42BC98FC2D09"
#define  UUID_OTA_UPDATE_STATUS  @"4D0500A0-766C-42C4-8944-42BC98FC2D09"

@interface SpectrumLockCharacteristics : NSObject
@property(readonly,nonatomic)NSDictionary *configForAdvertisementData;
@property(readonly,nonatomic)NSDictionary<NSString*,NSString*> *configForCharacSystemInfo;
@property(readonly,nonatomic)NSDictionary<NSString*,NSString*> *configForCharacSystemSettingsInfo;
@property(readonly,nonatomic)NSDictionary<NSString*,NSString*> *configForCharacSystemState;

@property (readonly, nonatomic) CBUUID *lockSystemCommandTx;
@property (readonly, nonatomic) CBUUID *lockSystem;
@property (readonly, nonatomic) CBUUID *lockSYSTEM_INFO;
@property (readonly, nonatomic) CBUUID *lockSystemState;
@property (readonly, nonatomic) CBUUID *lockSystemSettings;
@property (readonly, nonatomic) CBUUID *lockAccessCodes;
@property (readonly,nonatomic) CBUUID *lockEventLog;
@property(readonly,nonatomic)CBUUID *lockTimeRegion;
@property (readonly,nonatomic)CBUUID *lockActivation;
@property (readonly,nonatomic)CBUUID *lockIdentify;
@property(readonly,nonatomic)CBUUID *lockAuthentication;
@property(readonly, nonatomic)CBUUID *lockCommissioningInfo;
@property(readonly,nonatomic)CBUUID *lockBLE_System;
@property(readonly,nonatomic)CBUUID *lockBondedMobilDeviceList;
@property(readonly, nonatomic)CBUUID *lockAdvertiseSettings;
@property(readonly,nonatomic)CBUUID *lockConnectionSettings;
@property(readonly, nonatomic)CBUUID *lockTX_Power;
@property(readonly,nonatomic)CBUUID *lockBLEOTA;
@property(readonly,nonatomic)CBUUID *lockOTA_Update_Status;


@property(readonly,nonatomic)NSArray *command_TxLock;
@property(readonly,nonatomic)NSArray *command_TxUnlock;
@property(readonly,nonatomic)NSArray *command_TxReqCert;
@property(readonly,nonatomic)NSArray *command_TxReqChallenge;
@property(readonly,nonatomic)NSArray *command_TxSendChallenge;
@property(readonly,nonatomic)NSArray *command_TxSendSignature;
@property(readonly,nonatomic)NSArray *command_TxConfirmAuthentication;
@property(readonly,nonatomic)NSArray *identify;
@property(readonly, nonatomic)NSArray *command_TxSetTime;
@property(readonly, nonatomic)NSArray *command_TxSetFriendlyName;

@end
