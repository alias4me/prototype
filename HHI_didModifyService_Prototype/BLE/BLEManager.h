//
//  BLEManager.h
//  SkyLocks
//
//  Created by Manoj Champanerkar on 3/21/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CBCentralManager.h>
#import "BLE_Advertisement.h"
#import "BLEPeripheral.h"

#define BLEErrorNotification @"BLEError"
#define BLEManagerStateChangeNotification @"BLEManagerStateChange"

#define BLEPeripheralFoundNotification @"BLEPeripheralFound"
#define BLEPeripheralRefreshNotification @"BLEPeripheralRefresh"


#define BLEPeripheralConnectNotification @"BLEPeripheralConnect"
#define BLEPeripheralConnectErrorNotification @"BLEPeripheralConnectError"

#define BLEPeripheralDisconnectNotification @"BLEPeripheralDisconnect"

/*
@class BLEManager; // forward declaration

@protocol BLEManagerDelegate

@optional
-(void)didFailToConnectToPeripheral:(BLEPeripheral *)peripheral error:(NSError *)error;
@end
*/

@interface BLEManager : NSObject


@property (readonly,nonatomic) BLEPeripheral *activePeripheral;
@property (readonly, nonatomic) NSMutableArray <BLEPeripheral *> *discoveredPeripherals;
@property (readonly, nonatomic) CBCentralManager *manager;
@property(strong,nonatomic)NSMutableArray <NSUUID*>*peripheralsAlreadyInDatabase;

// @property (readony, nonatomic) BLEPeripherals *pairedPeripherals; TODO
//-(void)scanAll;
-(void)scanForNewLocks;
-(void)registerPeripheral:(BLEPeripheral *)peripheralToRegister;
//TODO scan specific ones -(void)scanPeripherals:(NSArray <NSUUID *>*)peripherals;
-(void)readAdvertisementForPeripheralWithIdentifyer:(NSMutableDictionary <NSUUID*, BLE_Advertisement*>*)PeripheralIdentifierAndAdvClass;
-(void)readAdvertisementForPeripheralWithDeviceID:(NSMutableDictionary <NSUUID*, BLE_Advertisement*>*)PeripheralIdentifierAndAdvClass;
-(void)connectToPeripheral:(BLEPeripheral *)peripheral SetAuthentificationToNotify:(BOOL)setAuthenticationToNotify;
-(void)disconnect:(BLEPeripheral *)peripheral;
-(void)stopScan;
-(CBPeripheral*)getPeripheralByIdentifier:(NSUUID *)PeripheralIdentifier;
-(void)setPeriphersToExcludeFromScans:(NSMutableArray <NSUUID*>*)PeripheralIdentifiersToExclud;
-(void)addPeripheralToExclude:(NSUUID*)uuidToExclude;
-(void)removePeriphrtalToExclure:(NSUUID*)uuidToStopExcluding;

@end
