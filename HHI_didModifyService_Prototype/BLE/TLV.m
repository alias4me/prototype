//
//  TLV.m
//  Spectrum
//
//  Created by Robert on 6/28/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "TLV.h"
#import "BLE_Data.h"


@interface TLV(){
    NSData *_packetValue,*_endValue;
    int _packetType;
    int _packetLength;

}
@end

@implementation TLV

-(id)initWithData:(NSData *)tlvData{
    if (self=[super init]) {
       
        [self splitTLVpacket:tlvData];
    }
    return self;
}

-(id)initWithDataAndUUID:(NSData *)tlvData characteristicUUID:(NSUUID *)uuid{
    if (self=[super init]) {
        [self getEndValue:uuid];
        [self splitTLVpacket:tlvData];
    }
    return self;
}


-(void)getEndValue:(NSUUID *)uuid{
    if ([uuid isEqual:SpectrumApplication.spectrumLockCharacterisitcs.lockBondedMobilDeviceList]) {
        unsigned char bytes[] = {0x00};
        _endValue =[NSData dataWithBytes:bytes length:1];
    }
}


-(void)splitTLVpacket:(NSData *)d{
    BLE_Data *encoder=[[BLE_Data alloc]init];
   
    if (d.length>0) {
        
        NSData *TLV_Type=[[NSData alloc]initWithData:[d subdataWithRange:NSMakeRange(0, 1)]]; // [NSString stringWithFormat:@"%@",d];
        NSString *typeStr=[encoder hexDataToString:TLV_Type]; //[[NSString alloc] initWithData:TLV_Type encoding:NSASCIIStringEncoding];
        //typeStr=[self fixHexString:typeStr];
        _packetType=[encoder readHexInt:typeStr isLittleEndian:YES]; // substringToIndex:1];

        if (d.length==2) {
            DDLogDebug(@"TLV type decoded to string= %@",typeStr);
            DDLogDebug(@"TLV type decoded to int= %d",[encoder readHexInt:typeStr isLittleEndian:YES]);
        }
        NSData *TLV_Length=[[NSData alloc]initWithData:[d subdataWithRange:NSMakeRange(1, 1)]];
     
        NSString *lengthStr=[encoder hexDataToString:TLV_Length];
       //lengthStr= [self fixHexString:lengthStr];
        _packetLength=[encoder readHexInt:lengthStr isLittleEndian:YES];
        if (_packetLength>0) {
            _packetValue=[[NSData alloc]initWithData:[d subdataWithRange:NSMakeRange(2, d.length-2)]];//];   [[NSData alloc]initWithData:[
            if (_endValue) {
                if ([[[NSData alloc]initWithData:[d subdataWithRange:NSMakeRange(d.length-1,1)]] isEqual:_endValue]) {
                    _packetValue=[[NSData alloc]initWithData:[_packetValue subdataWithRange:NSMakeRange(0,_packetValue.length-1)]];//];
                }
                
            }
            
        }else{
            _packetValue=[[NSData alloc]init];
        }

        // int TLV_ValueTemp=[decod readHexInt:[NSString stringWithFormat:@"%@",TLVvalueTemp] isLittleEndian:YES];
        DDLogDebug(@"%@", [NSString stringWithFormat:@"Type=%d Length=%d  Value=%@", _packetType, _packetLength, _packetValue]);
        
    }else{
        DDLogDebug(@"TLV data.length=0");
    }

}

-(int)packetValueToInt{
    BLE_Data *encoder=[[BLE_Data alloc]init];
    
    NSString *packetValueString=[encoder hexDataToString:_packetValue];
    return [encoder readHexInt:packetValueString isLittleEndian:NO];
    
}


-(NSString *)fixHexString:(NSString *)dateStr{
    dateStr=[dateStr stringByReplacingOccurrencesOfString:@"<" withString:@""];
    dateStr=[dateStr stringByReplacingOccurrencesOfString:@">" withString:@""];
    return dateStr;
}
@end
