//
//  FirstTimeSetupData.m
//  SkyLocks
//
//  Created by Robert on 4/9/18.
//  Copyright © 2018 Spectrum Brands. All rights reserved.
//

#import "UserSetupData.h"
#import "SPKDatabaseManager.h"
//#import "addUserToDB.h"

@interface UserSetupData()
{
    NSString *_firstName;
    NSString *_lastName;
    NSString *_email;
    NSString *_phoneNumber;
    NSString *_identityProvider;
    NSString *_cognitoID;
    
}
@end

@implementation UserSetupData

//@synthesize firstName;
//@synthesize lastName;
//@synthesize email;
//@synthesize phoneNumber;
//@synthesize cognitoID;

-(id)initWithData:(NSString *)usrCognitoID FirstName:(NSString *)usrFirstNm LastName:(NSString *)usrLastNm Email:(NSString *)usrEml Phone:(NSString *)usrPhoneNumb IdentityProvider:(NSString *)identityProvider{
    
    if (self=[super init]) {
        _firstName=usrFirstNm;
        _lastName=usrLastNm;
        _email=usrEml;
        _phoneNumber=usrPhoneNumb;
        _identityProvider=identityProvider;
        _cognitoID=usrCognitoID;
    }
    return  self;
}


-(void)updateDatabase{
    //SPKDatabaseManager *SpectrumDB=[[SPKDatabaseManager alloc]init];
    NSString *basePath=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    //replace @"tempMeta" and @"tempSync" with proper data. IDK what these arguments are for
    SPKDatabaseManager *SpectrumDB =[[SPKDatabaseManager alloc]initWithBasePath:basePath metaDBName:@"tempMeta" tranDBName:_cognitoID syncStatusDBName:@"tempSync"];
    
    
}

@end
