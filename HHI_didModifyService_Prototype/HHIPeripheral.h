//
//  HHIPeripheral.h
//  HHI_didModifyService_Prototype
//
//  Created by Robert on 3/8/19.
//  Copyright © 2019 Robert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HHICentralManager.h"

typedef enum {
    CharacteristicRead,
    CharacteristicWrite,
    CharacteristicScan,
    CharacteristicECDSA
} CharacteristicAction;

typedef  void (^SPPeripheralHandler) (NSData *data, NSError *error);
typedef void (^SPCharacteristicsHandler) (NSArray<CBCharacteristic *> *,  NSError *error);
typedef void (^SPCharacteristicReadHandler) (NSData *charValue,  NSError *error);


@interface HHIPeripheral : NSObject

@property (copy, nonatomic) SPPeripheralHandler peripheralHandler;
@property (copy, nonatomic) SPCharacteristicsHandler characteristicsHandler;

-(void)unlock;
-(void)connectHHILock:(HHICentralManager *)mgr;
-(void)scanServices:(CBPeripheral *)periph CharacteristicsHandler:(SPCharacteristicsHandler)characteristicsHandler;
-(void)readCharacteristics:(NSArray <NSString *>*)uuids ReadHandler:(SPCharacteristicReadHandler)readHandler;
-(void)writeToNothing;
-(void)writeToIndication;
-(void)ECDSA:(HHICentralManager *)hhiMgr;
@end
