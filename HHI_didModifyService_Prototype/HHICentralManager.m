//
//  HHICentralManager.m
//  HHI_didModifyService_Prototype
//
//  Created by Robert on 3/8/19.
//  Copyright © 2019 Robert. All rights reserved.
//

#import "HHICentralManager.h"
#import "HHIPeripheral.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "ViewController.h"

@interface HHICentralManager () <CBCentralManagerDelegate>{
    CBCentralManager *_mgr;
    CBPeripheral *_lockPeripheral;
    HHIPeripheral *periph;
    NSString *_attributeTableChanged;
    NSString *AdvName;
    NSArray <CBCharacteristic *>* lockCharacteristics;
    id sender;
    CharacteristicAction charAction;
    NSMutableArray <NSString *> *uuids;
}

@end

@implementation HHICentralManager

-(id)init{
    if (self == [super init]){
        _mgr = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
        lockCharacteristics=[[NSMutableArray alloc]init];
        uuids = [[NSMutableArray alloc]init];
    }
    return self;
}

-(void)crashBLE{
    //@try {
        [periph writeToNothing];
       // _mgr =nil;
        //_mgr=[CBManager init];
    /*} @catch (NSException *exception) {
        NSLog(@"exception");
    } @finally {
        NSLog(@"nothing");
    }*/
}

-(void)startScanning:(NSString *)lockName sender:(id)senderVC{
    AdvName=lockName;
    sender=senderVC;
    periph=[[HHIPeripheral alloc]init];
    [self scanAll:^(NSError *err){
        if (err) {
            NSLog(@"something went wrong");
        }
        NSLog(@"ds");
        /*
         [wself deleteAllAccessCodesWithCompletion:^(NSError *error) {
         [wself deactivate];
         }];
         */
    }];
}


-(void)scanAll:(SPHandler)handler{
    _bleHandler=handler;
    charAction=CharacteristicScan;
    [self performSelector:@selector(done) withObject:nil afterDelay:5];
    //[_mgr connectPeripheral:_lockPeripheral options:nil];
    [_mgr scanForPeripheralsWithServices:nil options:nil];
}

-(void)readCharacteristics:(NSMutableArray <NSString *>*)characteristicUUIDs{
    uuids=characteristicUUIDs;
}

-(void)done{
    if ([sender isKindOfClass:[ViewController class]]) {
        ViewController *vc=sender;
        [vc updateUI:nil];
    }
}

-(void)ECDSA{
    charAction=CharacteristicECDSA;
  //  [_mgr connectPeripheral:<#(nonnull CBPeripheral *)#> options:<#(nullable NSDictionary<NSString *,id> *)#> ];
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSString *stateErrorString = @"";
    
    switch (central.state)
    {
        case CBManagerStateUnsupported:
            stateErrorString = @"Bluetooth Low Energy is unsupported.";
            break;
        case CBManagerStateUnauthorized:
            stateErrorString = @"This app is not authorized to use Bluetooth Low Energy.";
            break;
        case CBManagerStatePoweredOff:
            stateErrorString = @"Bluetooth on this device is currently powered off.";
            break;
        case CBManagerStateResetting:
            stateErrorString = @"The Bluetooth Low Energy Manager is resetting; a state update is pending.";
            break;
        case CBManagerStatePoweredOn:
            stateErrorString = @"";
            
            break;
        case CBManagerStateUnknown:
            stateErrorString = @"The state of the Bluetooth Low Energy Manager is unknown.";
            break;
        default:
            stateErrorString = @"The state of the Bluetooth Low Energy Manager is unknown.";
    }
    
    
    
}


-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI {
    
    if ([advertisementData[CBAdvertisementDataLocalNameKey] isEqualToString:AdvName]) {
        NSLog(@"hhi lock found");
        _attributeTableChanged=[NSString stringWithFormat:@"Found %@ but no change", AdvName];
        
        _lockPeripheral=peripheral;
        [_mgr stopScan];
        [periph connectHHILock:self];
        //_lockPeripheral.delegate=self;
        //[mgr connectPeripheral:_lockPeripheral options:nil];
        
    }
    
    NSData   *mfgData   = [advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];
    if (mfgData){
        
    }
}


-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"did disconnect");
}


-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"did connect");

    //NSArray <CBCharacteristic *> *characteristics;
    if (charAction==CharacteristicScan) {
        [periph scanServices:peripheral CharacteristicsHandler:^(NSArray <CBCharacteristic *>*lockChars, NSError *err){
            if (err) {
                
            }else{
                self->lockCharacteristics=lockChars;
            }
        }];
    
    }else if (charAction==CharacteristicRead){
        
        
        [periph readCharacteristics:uuids ReadHandler:^(NSData *d,NSError *err){
            NSLog(d);
        }];
    }else if (charAction==CharacteristicECDSA){
        
      //  [periph ECDSA];
//        [periph readCharacteristics:nil ReadHandler:^(NSData *d,NSError *err){
  //          NSLog(d);
    //    }];
    }
}


-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    //Lock may be already paired while authenticating
    NSLog(@"failed to connect");
    
}


-(void)closeConnection{
    [_mgr cancelPeripheralConnection:_lockPeripheral];
    
}

-(void)unlock{
    [periph unlock];
}

-(void)writeIndication{
    [periph writeToIndication];
}

#pragma mark - CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray<CBService *> *)invalidatedServices
{
    _attributeTableChanged=@"Found change to Attribute Table";
    
    [self done];
    NSLog(@"FOUND MODIFIED SERVICES");
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    NSLog(@"updated notification state for %@", characteristic.UUID.UUIDString);
    
    
    
}

/*
-(void)peripheralIsReadyToSendWriteWithoutResponse:(CBPeripheral *)peripheral{
    //not prefered as it only works on iOS 11.0 and above.
    //    [_delegate characteristicIsReadyToWrite:characteristic Peripheral:self];
    
    
}



-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    NSLog(@"did write to service uuid  %@", characteristic.UUID.UUIDString);
    
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        NSLog(@"%@", [NSString stringWithFormat:@"Service Error:%@ code:%ld",error.description,(long)error.code]);
    }else{
        for (CBService *service in peripheral.services) {
            [peripheral discoverCharacteristics:nil forService:service];
        
        }
    }
        [_mgr cancelPeripheralConnection:_lockPeripheral];
    
        if ([sender isKindOfClass:[ViewController class]]) {
            ViewController *vc=sender;
            [vc updateUI:error];
        }
    }
    //    if ([sender respondsToSelector:@selector(done:)]) {
  //      [sender done:error];
    //}
//    [sender done:error];


-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (error) {
        NSLog(@"%@", [NSString stringWithFormat:@"Characteristic Error:%@ code:%ld",error.description,(long)error.code]);
    }else{
        for (CBCharacteristic *ch in service.characteristics) {
            NSLog([ch.UUID UUIDString]);
        }
    }
    
}





-(void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    // NSLog(@"hit descriptor");
}


-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error{
    NSLog(@"did update descriptor");
    
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    
    
}
*/
@end
